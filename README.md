# Little League Roster Builder

To help quickly build rosters for varying numbers of players, 
with rules requiring players to play different positions each inning, 
and allowing for those rules to be bent when there aren't enough players to enforce them,
this little app helps spin through permutations ensuring a fairly random spread of play.

## Build

Pull the project from version control.

The project is built using Gradle v8.
Building and running the app can be done with `./gradlew` as it has the default tasks of `clean build test` to ensure the build succeeds.

The project leverages [ShadowJar](https://github.com/johnrengelman/shadow) to build an executable JAR file.
This can be built using `./gradlew jar` to create the file in the project's build /lib folder.
Then the app can be executed by simply invoking Java with the resulting JAR file in the command line,
such as `java -jar build/libs/LLRosterBuilder.jar`, followed by any desired command line arguments.

## Running

The roster builder can be run using the bundled Gradle wrapper,
or after building the JAR using Java at the command line.
The examples assume execution from the root of the project;
full paths could be used instead.

### Gradle

To execute using Gradle, simply use the run task:

`./gradlew run`

To add any arguments, add the `--args` parameter and enclose the arguments to pass to the roster builder within quotes.
As some of the parameters use quotations, these arguments should be contained in single quotes:

`./gradlew run --args='-n 5'`

### Java

Using the shadowjar generated JAR requires Java and the command line parameters indicating which JAR to use:

`java -jar build/libs/LLRosterBuilder.jar`

Arguments are more simply added to the command line:

`java -jar build/libs/LLRosterBuilder.jar -n 5`

The following examples continue to use the JAR, which allows the JAR file to be distributed and makes only the assumption that Java is available to run it.

# Concepts

Our league tries to make teams of about 12 players.
When all players are in a game, not everyone can play every inning.
When not all players are in a game, it may be that not every position is filled.

The roster builder will attempt to balance the players and positions, and respect the little league rules.

The roster builder will try 5 times to successfully build a rule-following line-up.
During each try, it will try (a seemingly ridiculous) 10,000 times to successfully build each inning
(which is a lot short of the potential 362,880 combinations of 9 players in different positions).
It will also ensure that each position (as needed by the number of available players) is filled in each inning.

If it can't successfully build a roster, the app will error and show the last attempt.
Accept the last best try, or check the parameters and try again.

## Positions and Players

The roster won't allow fewer than 5 or more than 15 players on a team.
When players are added to the roster, they play in the same order each inning.
The little league rules require players to rotate positions; these rules are outlined below.

The roster uses a 10-player field, including both left and right center field positions, making four outfield positions.

For games with more than 10 players on a team, only 10 players are given a position each inning with additional players taking turns "on the bench."

For games with 9 players, the "left center field" is noted in the roster without a "right center," but should be used as a center field position.
This is what other baseball leagues play.

For games with fewer than 9 players, the catcher and outfield positions are removed, as necessary, to facilitate filling an infield and keeping a fuller outfield.

- A game with 8 or fewer players has no catcher.
- A 7 player team has only two outfielders.
- A 6 player team has only one outfielder.
- A 5 players team has just infield positions, other than the catcher.

The catcher is removed first as this is both generally the least desired position by players, and because there are more outfield plays than plays at home plate.
This does mean that a coach usually returns balls to the pitcher; in our league, that "catcher coach" isn't allowed to make any other plays.
Teams could decide to change a selected outfield (or other) position for a catcher instead, but this does not factor into the infield rule checking (which are usually rules suppressed when team sizes get too small anyway).

## Innings

Our league plays 6-inning games, so this is the default of no other quantity is provided.

The roster builder will build from 1 to 12 innings using the `--numberOfInnings` parameter.

Note selecting more than 6 innings will build a roster, but suppress all the rules as there will be too many innings to fill.

# Little League Rules and Considerations

These are the rules used in our Little League (and seem fairly standard from some casual searching).

Many can be overridden with parameters noted below.
Some may self-override if there aren't enough players to allow the rule to be maintained (also noted below).

## Rule #1: No more than 2 innings on the "bench"

This rule simply limits any bench innings for each player to 2 per game.

As the roster is built, players with innings that would violate this rule don't receive the bench as an option to consider.

**For teams with fewer than 11 players, there will be no bench players, so this rule is automatically suppressed as unnecessary.
As there are only 10 positions, teams with more than 13 players will suppress this rule, allowing more than 2 bench innings for some players as needed.**

## Rule #2: At least 2 innings playing in the infield

Strictly, this rule ensures each player gets two innings in the infield, to avoid being ostracised to the outfield.

This rule is validated after a complete set of innings is created.
Each player's set of innings is counted to ensure that they get at least 2 innings in the infield.

It's unlikely, but without the rule it could happen that a roster could be built where a player is in an outfield position or on the bench in each inning, perhaps with one or no innings in the infield.
This will trigger one of the retries to ensure this doesn't happen.

## Rule #3: No more than 2 innings in the same position

Strictly this rule simply checks to ensure a player does not repeat any positions more than twice at any time in a game.

As the roster is built, players who have already played a position twice won't receive that position as an option.

In a game with 9 or more players, there are enough player to keep the positions rolling so that no one needs replay any position a third time.

Although, in games with more players, they may have more than one bench inning (see `rule #1`).
This rule does not check bench innings because of rule 1.

**In games with 5 players, it's impossible to build a 6-inning roster with this rule, so it is automatically suppressed.**

## Rule #4: No more than 2 consecutive innings in the infield

To best ensure a fair shot at playing around the positions, the roster builder prevents too many innings in a row in the infield.

As the roster is built, positions played in the previous inning's position are removed as options to consider.
Note that "the bench" is skipped as "not a position," so the rule will check around bench innings.
This will prevent situations where a player is in the infield twice, on the bench, and then in the infield again
(or in the infield, on the bench, then in the infield twice more).

**Teams with fewer than 9 players will automatically suppress this rule, as there are not enough outfield positions to play.**

## Additional Rules

Discussed below, there are a couple added rules to help make the position distribution a little less "random" (although it's still random),
and adds a little more "fair" to the random.

Check out the *Back-to-back Check* and *Kinder Position Check* below.

# Arguments

The program will execute without any parameters and build a roster for nine (generically named and numbered) players for six innings.
The positions will be sufficiently randomized and follow the rules outlined above.

```
java -jar build/libs/LLRosterBuilder.jar 
 
++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 1           |  2B  |  SS  |  LC  |  SS  |   P  |  RF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 2           |  1B  |  RF  |  SS  |   C  |  RF  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 3           |  SS  |  2B  |  LF  |  3B  |  2B  |  LC  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |  3B  |   P  |  RF  |  1B  |  SS  |  LF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |   C  |  LF  |  1B  |   P  |  LF  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 6           |   P  |  LC  |  2B  |  2B  |  LC  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 7           |  RF  |  1B  |   C  |  RF  |   C  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 8           |  LF  |   C  |  3B  |  LC  |  1B  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 9           |  LC  |  3B  |   P  |  LF  |  3B  |   C  ||
++----+--------------------+------+------+------+------+------+------++
```

Notice the default 6-inning, 9-player game.

The game is too small to have bench positions, so rule 1 doesn't apply, but clearly no one has more than 2 bench innings.

All players have 2 infield innings, so rule 2 is met.

No player plays any position more than twice, so rule 3 is met.

Np player has more than 2 consecutive infield positions, so rule 4 is met.

Related to the additional rules, notice player 6 plays second base twice in a row, 
and player 9 plays third base before everyone else has had a chance (as is the case with player 6's second base, also).

Adding the parameters below will change the results according to the parameter.

## Help

Leveraging the `--help` (or its shorter version `-h`) parameter will display a helpful list of the parameters and brief descriptions.
When asking for help, all other parameters will be ignored.

Using a parameter that doesn't exist or is incorrect will also display the help list.

```
java -jar build/libs/LLRosterBuilder.jar -h  

Usage: LLRosterBuilder [-1234bchks] [--[no-]debug] [--grid] [--[no-]info] [--
                       [no-]io] [--simple] [--away=<away>] [-f=<failLimit>]
                       [--home=<home>] [-i=<numberOfInnings>] [-l=<label>]
                       [-n=<numberOfPlayers>] [-r=<retryLimit>]
                       [-np=<playerNamesNotAttending>[,
                       <playerNamesNotAttending>...]]... [-p=<playerNames>[,
                       <playerNames>...]]...
Quick line-up and position randomizer for little league baseball teams.
  -1, --no1                  Ignore rule #1, no more than 2 bench innings,
                               skipping fair levels
  -2, --no2                  Ignore rule #2, at least 2 infield innings,
                               skipping fair levels
  -3, --no3                  Ignore rule #3, no more than 2 innings in same
                               position
  -4, --no4                  Ignore rule #4, limit 2 consecutive infield innings
      --away=<away>          Add the away team for printing the scoreboard area
  -b, --backToBackCheck      Keep players from playing the same position twice
                               in a row
  -c, --commands             Print the command line parameters needed to run
                               again with the same batting order; positions may
                               end up different
      --[no-]debug           Turn on debug-level messaging, which can be very
                               chatty.
  -f, --fail=<failLimit>     Number of times to allow rule failures to be
                               retried in roster run
      --grid                 Print scoring grid without positions or names
  -h, --help                 Show help
      --home=<home>          Add the home team for printing the scoreboard area
  -i, --numberOfInnings=<numberOfInnings>
                             Number of innings to plan (1-15)
      --[no-]info            Turn on info-level messaging, which can be a
                               little chatty.
      --[no-]io, --[no-]inout
                             Print count of infield and outfield innings for
                               each player.
  -k, --kinderPositionCheck  Allow kinder position checking, keeping fair levels
  -l, --label=<label>        Header to add to printout
  -n, --numberOfPlayers=<numberOfPlayers>
                             Number of players to use (1-12)
      -np, --not-playing=<playerNamesNotAttending>[,
        <playerNamesNotAttending>...]
                             List of players not attending, but still listed on
                               the roster
  -p, --player=<playerNames>[,<playerNames>...]
                             List of players
  -r, --retry=<retryLimit>   Number of times to retry for full roster
  -s, --[no-]shuffle         Shuffle player order from input
      --simple               Put the batting order in position order for the
                               first inning
```

Notice that help lists the parameters in alphabetic order.
The order of the parameters is not important when running the builder.

The following discussion is in an order that seems important to a good run.

## Grid

Using the `--grid` parameter will print an empty grid.
No positions will be generated.
This will produce an "empty" grid that can be used to create a lineup and roster by hand.

The grid will be printed with the number of innings specified using the `--numberOfInnings` parameter, and for the number of players using the `--numberOfPlayers` parameters, or their default values.
The number of players will also respect any `--player` parameters, but the players will not be used in the final roster.
These parameters still respect their limits, even though no roster building will occur that would otherwise make the limits useful.

If provided, a `--label` will be added to the top, and the `--away` and `--home` teams and score area will be added to the bottom.
The column for the in/out count will be added if the `--inout` parameter is specified.

Any other provided parameter will be ignored but remembered.

For example, specifying only `--grid` will use the default 9 players and 6 innings to produce a simple 6x9 grid with spaces for player names and numbers.

```
java -jar build/libs/LLRosterBuilder.jar --grid

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    |                    |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
||    |                    |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
||    |                    |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
||    |                    |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
||    |                    |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
||    |                    |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
||    |                    |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
||    |                    |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
||    |                    |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
```

## Back-to-back Check

Leveraging the parameter `--backToBackCheck` (or its shorter version `-b`) will add logic that ensures players won't play the same position they played in the previous inning.

## Kinder Position Check

Leveraging the parameter `--kinderPositionCheck` (or its shorter version `-k`) will add logic that ensures that everyone has played a position before a player is allowed to play a position the second time.

Once all players have played a position, players can be set in that position again.

_Note: this was originally written to ensure an even leveling, but that logic was reduced for speed...it turns out to be pretty unlikely that third times will happen very often._

## Number of innings

Leveraging the `--numberOfInnings` parameter (or its shorter version `-i`) will change the number of innings to those desired.
Without the parameter, 6 innings is assumed.

No fewer than 1 inning is allowed, and no more than 12.

**Having more than 6 innings automatically suppresses all rule validations.
Using fewer than 6 innings may have unexpected adherence to the rules, but suppression is for the user to request.**

## Number of players

Leveraging the `--numberOfPlayers` parameter (or its shorter version `-n`) will change the number of players in the game.
Without the parameter, 9 players is assumed.

No fewer than 5 players is allowed, and no more than 15.

Our league has a minimum of 5 players to play in a game.
If you can't fill the infield, you can't play the tame.

There's no real restriction on having more than 15 players on a team.
It would result in a third or more of the players sitting on the bench.

Larger teams will surely result in repeated, and potentially back-to-back, positions.
Of course, with smaller teams having no outfield positions, all players also play lots of or only infield positions.

The rules note the team size needed to automatically suppress them.

```
java -jar  build/libs/LLRosterBuilder.jar -n 5

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #1          |  2B  |   P  |  1B  |  SS  |   P  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #2          |   P  |  2B  |  2B  |  1B  |  3B  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #3          |  3B  |  SS  |  SS  |  3B  |  1B  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #4          |  1B  |  1B  |   P  |  2B  |  2B  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #5          |  SS  |  3B  |  3B  |   P  |  SS  |   P  ||
++----+--------------------+------+------+------+------+------+------++
```

### With Player Names

The number of players can also be inferred from the `--player` parameter (below).
Adding players using the only `--player` parameter will set the number of players to at those provided.

If both players added using `--player` and a `--numberOfPlayers` is provided,
the list will either be expanded to include missing players (named "Player #") if the `--numberOfPlayers` parameter is larger than the provided list,
or the provided list will be truncated (in the order provided) to be limited to the number of players provided by the parameter.

There are some examples of this in the Player Names discussion.

## Shuffle

Leveraging the `--shuffle` parameter (or its shorter version `-s`) will shuffle the players in the batting order.

Without the shuffle parameter, the provided (or generated) batting order is assumed to be on purpose.

The `--no-shuffle` parameter can be used to suppress the feature.
It is not necessary to specify this, as it is the default, but is provided to allow command-line editing.

Notice in the previous example all the generated order.
Adding the parameter to the previous example will randomly order the players.

```
java -jar  build/libs/LLRosterBuilder.jar -s

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 9           |  LC  |  3B  |   C  |  LF  |  SS  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 6           |  1B  |  2B  |  LF  |  1B  |   C  |  RF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 8           |  SS  |  LC  |  2B  |   C  |  LC  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 1           |  2B  |  LF  |  3B  |  3B  |  LF  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 7           |  3B  |  1B  |  RF  |  2B  |  3B  |  LC  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 3           |  RF  |   C  |  1B  |  RF  |   P  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 2           |  LF  |  SS  |  SS  |  LC  |  1B  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |   C  |  RF  |   P  |   P  |  RF  |   C  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |   P  |   P  |  LC  |  SS  |  2B  |  LF  ||
++----+--------------------+------+------+------+------+------+------++
```

Entering the players in the list (discussed below) in the desired batting order will be retained without the shuffle parameter.
Entering players and adding the shuffle parameter will shuffle the entered names.

## Simple Order

To help ease into the game positions shuffling, using the `--simple` parameter puts the first inning in the order of the positions to play.

```
java -jar  build/libs/LLRosterBuilder.jar --simple

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 1           |   P  |  SS  |  LC  |  SS  |  3B  |  RF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 2           |   C  |  LC  |  3B  |  3B  |  LF  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 3           |  1B  |   P  |  LF  |   C  |   P  |  LC  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |  2B  |  RF  |  1B  |  1B  |  RF  |   C  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |  3B  |  2B  |  RF  |   P  |  SS  |  LF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 6           |  SS  |  LF  |   C  |  2B  |  LC  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 7           |  LF  |   C  |  SS  |  LF  |   C  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 8           |  LC  |  1B  |   P  |  RF  |  2B  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 9           |  RF  |  3B  |  2B  |  LC  |  1B  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
```

Notice in the following examples that adding the `--shuffle` parameter rearranges the order of the batters, 
while combined with the `--simple` parameter keeps the first inning in position order.

```
java -jar  build/libs/LLRosterBuilder.jar --simple -s

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 9           |   P  |   C  |  LF  |   P  |  SS  |  LC  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |   C  |  2B  |  LC  |   C  |   P  |  LF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 2           |  1B  |  LC  |   C  |  3B  |  LC  |   C  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |  2B  |  RF  |  2B  |  1B  |  LF  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 3           |  3B  |  3B  |  RF  |  SS  |   C  |  RF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 8           |  SS  |  LF  |  3B  |  2B  |  RF  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 1           |  LF  |  SS  |  SS  |  LC  |  3B  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 6           |  LC  |   P  |  1B  |  RF  |  1B  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 7           |  RF  |  1B  |   P  |  LF  |  2B  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
```

In cases where there aren't enough players to fill all the positions and the catcher is removed, it is appropriately skipped.

```
java -jar  build/libs/LLRosterBuilder.jar --simple -n 7 

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 1           |   P  |  2B  |  SS  |  3B  |  RF  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 2           |  1B  |  SS  |  3B  |  LF  |  LF  |  RF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 3           |  2B  |  LF  |  1B  |  SS  |  SS  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |  3B  |  RF  |  RF  |  1B  |  1B  |  LF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |  SS  |  3B  |   P  |  2B  |   P  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 6           |  LF  |   P  |  LF  |  RF  |  2B  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 7           |  RF  |  1B  |  2B  |   P  |  3B  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
```

In the cases where there are bench players, everyone after the tenth player will be on the bench in the roster.

```
java -jar  build/libs/LLRosterBuilder.jar --simple -n 15 

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 1           |   P  |  RF  |   B  |  2B  |  2B  |   B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 2           |   C  |  2B  |   B  |  LC  |   B  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 3           |  1B  |  RC  |  RF  |  3B  |  SS  |  RC  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |  2B  |   B  |  RC  |  1B  |   B  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |  3B  |   B  |   P  |   B  |  1B  |   B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 6           |  SS  |   B  |   C  |   B  |  LF  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 7           |  LF  |   B  |  LF  |   B  |  RC  |   C  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 8           |  LC  |  LC  |  2B  |  RC  |   P  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 9           |  RF  |   B  |  1B  |   C  |   B  |  LC  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 10          |  RC  |  1B  |  SS  |   B  |  LC  |   B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 11          |   B  |  3B  |  LC  |   B  |   C  |   B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 12          |   B  |  LF  |   B  |   P  |  RF  |   B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 13          |   B  |  SS  |   B  |  RF  |  3B  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 14          |   B  |   C  |   B  |  SS  |   B  |  LF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 15          |   B  |   P  |  3B  |  LF  |   B  |  RF  ||
++----+--------------------+------+------+------+------+------+------++
```

## Player Names

Leveraging the `--player` parameter (or its shorter version `-p`) will give the roster player names and optional jersey numbers.
When not provided, generic players in the form of "Player #" will be used instead.

If player names are provided, the parser will assume the list contains all players on the roster.
If additional players are desired, or the number of players should be reduced, also use the `numberOfPlayers` parameter
to add or limit the players, as mentioned in the number of players discussion above.

### Spaces in Names
Player names can include spaces by either escaping them or wrapping the name in quotation marks, such as "Babe Ruth"
Player names can be provided in separate parameters, or by a comma separated list, or a combination.
For example, these are equivalent:

`java -jar  build/libs/LLRosterBuilder.jar -p "Babe Ruth" -p "Hank Aaron" -p "Ty Cobb"`

`java -jar  build/libs/LLRosterBuilder.jar -p Babe\ Ruth -p "Hank Aaron" -p "Ty Cobb"`

`java -jar  build/libs/LLRosterBuilder.jar -p "Babe Ruth","Hank Aaron" -p "Ty Cobb"`

`java -jar  build/libs/LLRosterBuilder.jar -p "Babe Ruth","Hank Aaron","Ty Cobb"`

These examples won't really work, as they don't hit the minimum of 5 players for the roster.

At least 5 players needs to be in the list to build a roster with just player names.
Adding the parameter for number of players will allow the roster to be built, 
filling in with the placeholder names used when not providing player names.

```
java -jar  build/libs/LLRosterBuilder.jar -p "Babe Ruth" -p "Hank Aaron" -p "Ty Cobb" -n 5

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Babe Ruth          |  2B  |  1B  |  SS  |  2B  |  2B  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Hank Aaron         |  SS  |  3B  |  2B  |  3B  |  1B  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Ty Cobb            |  3B  |   P  |  3B  |   P  |  SS  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |  1B  |  SS  |  1B  |  SS  |  3B  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |   P  |  2B  |   P  |  1B  |   P  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
```

### Jersey Numbers in Names

Player jersey numbers can be included in the player name by preceding them with the # symbol,
as in the following example's `"Babe Ruth#3"`, which the parser will separate into the name and number appropriately.
Forgetting the # symbol will leave the number with the name in the Player field when the roster is created, as the error in `"Hank Aaron 44"` demonstrates.

```
java -jar  build/libs/LLRosterBuilder.jar -p "Babe Ruth#3" -p "Hank Aaron 44" -p "Ty Cobb" -n 5 

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||  3 | Babe Ruth          |  2B  |  3B  |  SS  |  2B  |  3B  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Hank Aaron 44      |  3B  |  SS  |  2B  |  3B  |  1B  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Ty Cobb            |  1B  |   P  |   P  |   P  |  2B  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |  SS  |  2B  |  3B  |  1B  |  SS  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |   P  |  1B  |  1B  |  SS  |   P  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
```
Cobb played before jersey numbers were a thing, so his isn't missing; he never had one.

### Filtering Positions

It can happen that some players can't play some positions, or should maybe have some positions removed for safety.
An easy example is that players without proper protection (e.g., not wearing a cup) can't play catchers.

Adding a filter to a player's name will remove that position from consideration when trying to find playable positions for that player.
This does require adding a player's name, but they can be the placeholder names, too.

Separating the list of positions from the player name (and optional number) with a semicolon will provide positions the player cannot play.
Multiple positions can be listed, preceding each with a semicolon.

```
java -jar  build/libs/LLRosterBuilder.jar -p "No Catcher;C" -p "Numbered Pitcher#2;2B" -p "No C or P;P;C" -n 10

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | No Catcher         |  LF  |  1B  |   P  |  RC  |  2B  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||  2 | Numbered Pitcher   |  RF  |  RC  |  SS  |  LC  |  LF  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | No C or P          |  LC  |  2B  |  1B  |  RF  |  SS  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |   C  |  SS  |  LF  |  LF  |  1B  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |  SS  |  RF  |  3B  |   P  |  RF  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 6           |  3B  |   C  |  RC  |  2B  |   P  |  RC  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 7           |  2B  |  LF  |  2B  |   C  |  LC  |   C  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 8           |  RC  |  LC  |   C  |  3B  |  RC  |  RF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 9           |  1B  |  3B  |  LC  |  SS  |   C  |  LC  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 10          |   P  |   P  |  RF  |  1B  |  3B  |  LF  ||
++----+--------------------+------+------+------+------+------+------++
```

Here we can see the first batter has been excluded from playing Catcher.
The second batter has been excluded from playing Pitcher, but demonstrates including a uniform number.
The third batter has been excluded from playing either Pitcher or Catcher.

In small enough games, or with enough exclusions, rosters may fail to be built.

Incorrectly identifying a position will result in an ignored exclusion.
Repeating positions will not affect the exclusions; the position will still be excluded.


## Not Playing Names

Leveraging the `--not-playing` parameter (or its shorter version `-np`) will add players to the roster but not find positions for them to play.

This might be helpful if players are uncommitted, or when it is desired to keep all players in mind when reviewing rosters.
Not-playing players do not factor into the player count for determining whether the rules can be used.

```
java -jar  build/libs/LLRosterBuilder.jar -p "Babe Ruth#3","Hank Aaron 44" -np "Ty Cobb" -n 5

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||  3 | Babe Ruth          |  SS  |  2B  |  1B  |  2B  |  2B  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Hank Aaron 44      |  3B  |  1B  |  SS  |  SS  |  1B  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 3           |  2B  |  3B  |   P  |  3B  |  SS  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |   P  |  SS  |  3B  |   P  |  3B  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |  1B  |   P  |  2B  |  1B  |   P  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Ty Cobb            |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
```

See how Ty Cobb was removed from the playing list, but still listed on the roster.
Also notice that there are still 5 players with positions, as requested.

### Spaces and Numbers in Names

As with players, player names with spaces can be used when surrounded by quotes,
numbers can be added by including them separated with the # symbol,
and the parameter may be used multiple times or joined with commas.

Additionally, regardless of the order provided, the list of players not playing should be ordered alphabetically (by the first letters, not appropriately by last name).

### Filtering Positions

Filtered positions will be created for players not playing, but won't affect the outcome of the roster as those players are never evaluated for positions.

This allows simply changing the `-p` to a `-np` to exclude a player.

The following shows the previous example with none of the players playing.

```
java -jar  build/libs/LLRosterBuilder.jar -np "No Catcher;C" -np "Numbered Pitcher#2;2B" -np "No C or P;P;C" -n 5

++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 1           |   P  |  3B  |  3B  |  1B  |  SS  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 2           |  3B  |  SS  |  2B  |  2B  |  1B  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 3           |  SS  |   P  |   P  |   P  |  3B  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |  2B  |  2B  |  SS  |  SS  |   P  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |  1B  |  1B  |  1B  |  3B  |  2B  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | No C or P          |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
||    | No Catcher         |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
||  2 | Numbered Pitcher   |      |      |      |      |      |      ||
++----+--------------------+------+------+------+------+------+------++
```

## Label

A header can be added to the output using the `--label` (or its shorter `-l` version) parameter.
This text will simply be added to the top of the roster.

```
java -jar  build/libs/LLRosterBuilder.jar -l "Imaginary game for docs"

++-------------------------------------------------------------------++
||                      Imaginary game for docs                      ||
++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #1          |  1B  |  LF  |  2B  |  3B  |  RF  |   C  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #2          |  2B  |  LC  |   P  |   C  |  LF  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #3          |  3B  |   C  |  LF  |  SS  |  1B  |  LC  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #4          |  SS  |  1B  |  RF  |  2B  |   C  |  LF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #5          |  LF  |   P  |   C  |  RF  |  3B  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #6          |   C  |  RF  |  SS  |   P  |  LC  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #7          |  RF  |  SS  |  3B  |  LC  |  2B  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #8          |  LC  |  2B  |  1B  |  LF  |   P  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player #9          |   P  |  3B  |  LC  |  1B  |  SS  |  RF  ||
++----+--------------------+------+------+------+------+------+------++
```

## Score Block

If both `--away` and `--home` parameters are provided, a little scorecard will be built at the bottom with room to tabulate the runs.
Like other parameters, names with spaces must be escaped or wrapped in quotation marks.

Both values are required for the block to appear.
If either team name is omitted, the score block will not be provided.

By default, this will also add a label at the header to further indicate the team order.

```
java -jar build/libs/LLRosterBuilder.jar --away "Away Team" --home "Home Team"

++-------------------------------------------------------------------++
||                 ** Away Team * at * Home Team **                  ||
++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 1           |  RF  |   P  |  1B  |  LC  |  SS  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 2           |  LF  |   C  |  3B  |  RF  |  1B  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 3           |  2B  |  3B  |  RF  |  1B  |   P  |  LF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |  LC  |  2B  |   P  |  LF  |  3B  |   C  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |  SS  |  LF  |  2B  |   C  |  LC  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 6           |  1B  |  SS  |  LC  |  3B  |   C  |  RF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 7           |  3B  |  RF  |   C  |   P  |  LF  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 8           |   C  |  LC  |  SS  |  2B  |  RF  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 9           |   P  |  1B  |  LF  |  SS  |  2B  |  LC  ||
++====+====================+======+======+======+======+======+======++
|| Away Team               |      |      |      |      |      |      ||
++-------------------------+------+------+------+------+------+------++
|| Home Team               |      |      |      |      |      |      ||
++-------------------------+------+------+------+------+------+------++
```

If a `--label` is provided, it will override the generated label.

If the `--inout` parameter is provided, an extra cell at the end of the score row will be created to match the column 
(and optionally enter score totals).

## Infield/Outfield

Leveraging the `--inout` parameter (or its shorter version `--io`) will add a count of infield and outfield positions, and number of bench innings, for each player.
This will give a quick check to ensure a fair distribution of the fielding.

The `--no-inout` or `--no-io` parameters can be used to suppress the banner.
It is not necessary to specify this, as it is the default, but is provided to allow command-line editing.

```
java -jar build/libs/LLRosterBuilder.jar --io

++----+--------------------+------+------+------+------+------+------+----------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  | In/Out/B ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player #1          |  RF  |  2B  |  SS  |  LC  |  1B  |   P  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player #2          |   C  |  LC  |   P  |  1B  |  LF  |  3B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player #3          |  LF  |   P  |  2B  |  RF  |  3B  |  1B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player #4          |   P  |   C  |  LC  |  2B  |  SS  |  RF  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player #5          |  1B  |  SS  |  LF  |   P  |  2B  |  LC  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player #6          |  LC  |  3B  |   C  |  LF  |   P  |  2B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player #7          |  SS  |  1B  |  RF  |  3B  |   C  |  LF  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player #8          |  3B  |  LF  |  1B  |   C  |  RF  |  SS  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player #9          |  2B  |  RF  |  3B  |  SS  |  LC  |   C  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
```

Here we can see each player has 4 infield innings, 2 outfield innings, and no bench innings.

## Command Line Recovery

Using the `--commands` parameter (or it's shorter `-c`) will cause the builder to emit the set of command parameters necessary to recreate the run.
This can be useful if some of the richer options are used, like labels, teams, and player names.
The emitted string can be reused to recreate the roster with those details.

Re-running the command with those parameters maintains the output and batting order, but not the player positions (outside of large random luck).

```
java -jar  build/libs/LLRosterBuilder.jar --io --backToBackCheck --kinderPositionCheck  --label "Label" --away "Away" --home "Home" -s -c

++------------------------------------------------------------------------------++
||                                    Label                                     ||
++----+--------------------+------+------+------+------+------+------+----------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  | In/Out/B ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 7           |  RF  |  1B  |  SS  |  LF  |  3B  |  2B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 6           |  2B  |   C  |  LF  |  1B  |  SS  |  LC  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 2           |  LC  |  3B  |  1B  |  RF  |   P  |   C  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 3           |  LF  |  SS  |  3B  |  LC  |  2B  |   P  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 9           |   P  |  LC  |   C  |  3B  |  RF  |  1B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 8           |  SS  |  2B  |  RF  |   C  |  1B  |  LF  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 4           |  3B  |  LF  |  2B  |   P  |  LC  |  SS  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 1           |  1B  |   P  |  LC  |  2B  |   C  |  RF  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 5           |   C  |  RF  |   P  |  SS  |  LF  |  3B  |  4/2/0   ||
++====+====================+======+======+======+======+======+======+==========++
|| Away                    |      |      |      |      |      |      |          ||
++-------------------------+------+------+------+------+------+------+----------++
|| Home                    |      |      |      |      |      |      |          ||
++-------------------------+------+------+------+------+------+------+----------++
-n 9 -i 6 -r 5 -f 10000 --io -b -k -l "Label" --away "Away" --home "Home" -p "Player 7" -p "Player 6" -p "Player 2" -p "Player 3" -p "Player 9" -p "Player 8" -p "Player 4" -p "Player 1" -p "Player 5"
```

The short versions of the labels are used when possible.

Some parameters are emitted even though they're defaults, 
as that part of the app that rebuilds the command line doesn't know if those were used from default or the command line

Importantly, if the `--shuffle` parameter is provided,
it is not emitted by this command, as the players are emitted in the order resulting from the shuffle.
This was done to allow building a roster with the same batting order each time;
it doesn't help to save the order and then shuffle again.

If shuffling again is desired, add the `-s` to the provided entry.
This can be useful when wanting to save the players, but not the order.

Neither is the `--command` parameter emitted, so it should be added if it is desired again.

The number of players and innings is always emitted even if they are the defaults or correctly inferred from the provided players.
Also, the retry and failure parameters are always provided.
they could be omitted to keep the defaults.

```
java -jar  build/libs/LLRosterBuilder.jar -n 9 -i 6 -r 5 -f 10000 --io -b -k -l "Label" --away "Away" --home "Home" -p "Player 7" -p "Player 6" -p "Player 2" -p "Player 3" -p "Player 9" -p "Player 8" -p "Player 4" -p "Player 1" -p "Player 5"     

++------------------------------------------------------------------------------++
||                                    Label                                     ||
++----+--------------------+------+------+------+------+------+------+----------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  | In/Out/B ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 7           |  LC  |   P  |  SS  |  LF  |   C  |  3B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 6           |  RF  |  SS  |   C  |  LC  |  3B  |  1B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 2           |  LF  |  1B  |  2B  |  RF  |  SS  |   P  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 3           |   C  |  LF  |  1B  |   P  |  RF  |  SS  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 9           |  2B  |  LC  |   P  |  SS  |  LF  |   C  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 8           |  1B  |  RF  |  3B  |   C  |  LC  |  2B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 4           |  SS  |  2B  |  LC  |  3B  |   P  |  LF  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 1           |   P  |  3B  |  LF  |  2B  |  1B  |  RF  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 5           |  3B  |   C  |  RF  |  1B  |  2B  |  LC  |  4/2/0   ||
++====+====================+======+======+======+======+======+======+==========++
|| Away                    |      |      |      |      |      |      |          ||
++-------------------------+------+------+------+------+------+------+----------++
|| Home                    |      |      |      |      |      |      |          ||
++-------------------------+------+------+------+------+------+------+----------++
 ```

**The positions are always randomized in each run. 
There is not (currently) a way to recollect the positions between runs.**

## Ignore rule #1

Leveraging the parameter `--no1` (or its shorter version `-1`) will suppress the bench inning check regardless of team size.

This could result in games where players are on the bench more than twice in a game.

This rule only applies to teams with more than 10 players, as there are no bench players otherwise.

It is incredibly rare that this could happen in a game with 11 or 12 players.

This rule is automatically suppressed when teams have 13 or more players, 
as someone will have to be on the bench a third time as there aren't enough positions to fill a roster.

Suppressing the rule for 11 or 12 player games offers a trivial performance improvement as the validation isn't performed.
Largely the rule suppression was necessary for teams with more than 12 players, so since it was enabled, exposing it was simple.

**Practically, this rule suppression is only necessary in the automatic cases for large teams.**

## Ignore rule #2

Leveraging the parameter `--no2` (or its shorter version `-2`) will suppress the infield inning check regardless of team size.

Here's an example with the rule suppressed.
We can see that some players have only one infield inning while other players have more.

```
java -jar  build/libs/LLRosterBuilder.jar -n 12 --io -2

++----+--------------------+------+------+------+------+------+------+----------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  | In/Out/B ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 1           |  3B  |  LF  |   C  |  1B  |   B  |  RF  |  3/2/1   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 2           |  RC  |  RF  |   B  |  RF  |  LC  |  SS  |  1/4/1   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 3           |  2B  |   B  |  1B  |  LF  |   C  |   P  |  4/1/1   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 4           |   C  |   P  |  LC  |  2B  |  LF  |  RC  |  3/3/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 5           |   B  |  3B  |  3B  |   B  |  RC  |   C  |  3/1/2   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 6           |   B  |  LC  |  RC  |   P  |   P  |  LC  |  2/3/1   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 7           |  LF  |  1B  |   P  |  RC  |  2B  |  3B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 8           |   P  |  SS  |   B  |  LC  |  3B  |  1B  |  4/1/1   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 9           |  LC  |   C  |  2B  |   B  |  RF  |  2B  |  3/2/1   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 10          |  1B  |   B  |  RF  |  3B  |  SS  |   B  |  3/1/2   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 11          |  SS  |  RC  |  SS  |   C  |   B  |  LF  |  3/2/1   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 12          |  RF  |  2B  |  LF  |  SS  |  1B  |   B  |  3/2/1   ||
++----+--------------------+------+------+------+------+------+------+----------++
```

Notice the second batter only has one infield position each.

**Practically, this is unlikely to happen until a large-enough team is attempted.**

## Ignore rule #3

Leveraging the parameter `--no3` (or its shorter version `-3`) will suppress the same-position check regardless of team size.

This could result in games where players are in the same position more than twice.

```
java -jar  build/libs/LLRosterBuilder.jar --io -3 

++----+--------------------+------+------+------+------+------+------+----------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  | In/Out/B ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 1           |  SS  |  1B  |  RF  |  3B  |  SS  |  LF  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 2           |  3B  |  3B  |  LC  |  1B  |   C  |  LC  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 3           |  2B  |   C  |  LF  |  SS  |  1B  |  RF  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 4           |   P  |  LC  |  1B  |   P  |  LF  |  1B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 5           |  LF  |  2B  |  2B  |  RF  |  3B  |  2B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 6           |  RF  |  SS  |   C  |  LC  |  2B  |   P  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 7           |  1B  |  RF  |  SS  |  2B  |  RF  |   C  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 8           |   C  |  LF  |  3B  |   C  |  LC  |  SS  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 9           |  LC  |   P  |   P  |  LF  |   P  |  3B  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
```

Notice the ninth batter is pitching in three innings.


## Ignore rule #4

Leveraging the parameter `--no4` (or its shorter version `-4`) will suppress the consecutive infield check regardless of team size.

This could result in games where a player plays more often in the infield,
including entirely playing in the infield.

```
java -jar  build/libs/LLRosterBuilder.jar --io -4

++----+--------------------+------+------+------+------+------+------+----------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  | In/Out/B ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 1           |  SS  |  3B  |  SS  |  LC  |  3B  |  2B  |  5/1/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 2           |  LF  |  RF  |  LF  |  3B  |   P  |  1B  |  3/3/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 3           |  3B  |  2B  |  2B  |  LF  |   C  |  LF  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 4           |  2B  |  1B  |   P  |  SS  |  1B  |  LC  |  5/1/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 5           |   P  |  LC  |   C  |  1B  |  2B  |   C  |  5/1/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 6           |  RF  |  SS  |  3B  |   C  |  LC  |  SS  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 7           |  LC  |   C  |  RF  |  2B  |  RF  |   P  |  3/3/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 8           |  1B  |  LF  |  LC  |   P  |  LF  |  3B  |  3/3/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
||    | Player 9           |   C  |   P  |  1B  |  RF  |  SS  |  RF  |  4/2/0   ||
++----+--------------------+------+------+------+------+------+------+----------++
```

Notice several players are in the infield a lot of times in a row.

## Debug

Leveraging the `--debug` parameter will output the information and decisions as the application runs.
This could be interesting to see how the decisions are made.
It does not change the output of the roster at the end.

Using the `--no-debug` parameter will ensure the debug output is suppressed.
It is not necessary to specify this, as it is the default, but is provided to allow command-line editing.

## Info

Leveraging the `--info` parameter will output additional information as the application runs.
It does not change the output of the roster at the end.

Using the `--no-info` parameter will ensure the info output is suppressed.
It is not necessary to specify this, as it is the default, but is provided to allow command-line editing.

Specifying both the `info` and `debug` parameters will result in debug output, which includes info output.

## Retry Limit

By default, the builder will try 5 times to successfully build a roster with the options and information provided.
Adding the `--retry` (or its shorter version `-r`) will allow specifying how may times to retry.
Specifying a retry limit of less than 1 will result in no tries.

```
java -jar  build/libs/LLRosterBuilder.jar -r 1

Unable to fill a complete roster. This is the last attempt.
++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 1           |  2B  |  LC  |  2B  |  3B  |  RF  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 2           |  SS  |  2B  |  LF  |  LC  |   C  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 3           |   P  |  3B  |  LC  |  2B  |  3B  |  LF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |  3B  |  1B  |      |   C  |   P  |  LC  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |  1B  |   C  |      |  RF  |  2B  |  2B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 6           |  LC  |  LF  |  1B  |  SS  |  LC  |   C  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 7           |   C  |   P  |  RF  |  1B  |  SS  |  RF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 8           |  RF  |  SS  |   C  |  LF  |  1B  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 9           |  LF  |  RF  |  SS  |   P  |  LF  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
```

Note a roster was attempted, but it was unable to fill all the positions in the third inning as those players (and others) already had 2 infield innings causing a violation of rule 4 after the other players also had been given the outfield innings.

## Failure Limit

By default, the builder will try 10,000 times to build each inning in the roster, for each retry.

Adding the `--fail` (or its shorter version `-f`) will allow specifying how many times to allow failures in each retry.
Specifying a failure limit of less than 1 will stop at the first position failure.

```
java -jar  build/libs/LLRosterBuilder.jar -f 0

Unable to fill a complete roster. This is the last attempt.
++----+--------------------+------+------+------+------+------+------++
|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 1           |  3B  |  SS  |      |   C  |  SS  |  LF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 2           |  SS  |   P  |  LC  |  3B  |  2B  |  LC  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 3           |  1B  |  3B  |      |  2B  |  1B  |  RF  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 4           |   P  |  1B  |      |   P  |  LC  |  3B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 5           |   C  |  LC  |  RF  |  LF  |  3B  |   P  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 6           |  LC  |  RF  |   C  |  SS  |  LF  |   C  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 7           |  2B  |  LF  |  SS  |  RF  |   C  |  1B  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 8           |  RF  |   C  |  1B  |  LC  |  RF  |  SS  ||
++----+--------------------+------+------+------+------+------+------++
||    | Player 9           |  LF  |  2B  |  LF  |  1B  |   P  |      ||
++----+--------------------+------+------+------+------+------+------++
```

Fundamentally this simply causes the app to run faster, but does result in more failures.
Functionally, it is usually the case that if a roster cannot be completed in the default number of failures, it is retried, and tends to be successful in the first handful of tries.

**It seems counterintuitive to allow the app to fail, but given the random nature used to try to build the rosters, failure needs to be allowed.
Not allowing the app to fail will result in a lot of unsuccessful roster attempts.
Adjust this only if it seems beneficial for limiting the time needed to build rosters.**
