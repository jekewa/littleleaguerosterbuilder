package com.swbyjeff.llrosterbuilder

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.Logger
import com.swbyjeff.llrosterbuilder.domain.Inning
import com.swbyjeff.llrosterbuilder.domain.Player
import com.swbyjeff.llrosterbuilder.domain.PlayerPosition
import com.swbyjeff.llrosterbuilder.domain.Position
import org.slf4j.LoggerFactory
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import static com.swbyjeff.llrosterbuilder.domain.Position.PITCHER
import static com.swbyjeff.llrosterbuilder.domain.Position.CATCHER
import static com.swbyjeff.llrosterbuilder.domain.Position.FIRST
import static com.swbyjeff.llrosterbuilder.domain.Position.SECOND
import static com.swbyjeff.llrosterbuilder.domain.Position.THIRD
import static com.swbyjeff.llrosterbuilder.domain.Position.SHORT
import static com.swbyjeff.llrosterbuilder.domain.Position.LEFT
import static com.swbyjeff.llrosterbuilder.domain.Position.LEFT_CENTER
import static com.swbyjeff.llrosterbuilder.domain.Position.RIGHT_CENTER
import static com.swbyjeff.llrosterbuilder.domain.Position.RIGHT
import static com.swbyjeff.llrosterbuilder.domain.Position.BENCH

@Unroll
class LLRosterBuilderCommandSpec extends Specification {

  // TODO: Mock LLRosterBuilder
  def llRosterBuilderCommand = new LLRosterBuilderCommand(numberOfInnings: 6, llRosterBuilder: new LLRosterBuilder())

  @Shared
  def thirteenPlayers = (1..13).collect { new Player("Player $it", it) }

  @SuppressWarnings('GroovyAccessibility')
  def setup() {
    // main.startDebugging()
    llRosterBuilderCommand.log.debug("TEST: ${specificationContext.currentIteration.displayName}")

    llRosterBuilderCommand.retryLimit = 2
    llRosterBuilderCommand.failLimit = 2
  }

  def 'recreates command line = #description'() {
    given:
    llRosterBuilderCommand.away = away
    llRosterBuilderCommand.backToBackCheck = back2back
    llRosterBuilderCommand.debug = debug
    llRosterBuilderCommand.home = home
    llRosterBuilderCommand.info = info
    llRosterBuilderCommand.label = label
    llRosterBuilderCommand.kinderPositionCheck = kinder
    llRosterBuilderCommand.numberOfInnings = innings
    llRosterBuilderCommand.numberOfPlayers = players
    llRosterBuilderCommand.order = order
    llRosterBuilderCommand.playerNamesNotAttending = notAttending
    llRosterBuilderCommand.printInOut = inOut
    llRosterBuilderCommand.skipRule1 = skipRule1
    llRosterBuilderCommand.skipRule2 = skipRule2
    llRosterBuilderCommand.skipRule3 = skipRule3
    llRosterBuilderCommand.skipRule4 = skipRule4
    llRosterBuilderCommand.simple = simple
    llRosterBuilderCommand.grid = grid

    expect: 'Note some parameters cannot be as listed here because of other parsing and rules'
    expected == llRosterBuilderCommand.recreateCommandLine()

    where:
    description             | players | innings | info  | debug | inOut | back2back | kinder | skipRule1 | skipRule2 | skipRule3 | skipRule4 | simple | grid  | label            | away   | home   | order                           | notAttending                                                || expected
    'no players'            | 9       | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | []                              | []                                                          || '-n 9 -i 6 -r 2 -f 2'
    'named player'          | null    | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | thirteenPlayers.take(1)         | []                                                          || '-n 1 -i 6 -r 2 -f 2 -p "Player 1#1"'
    'player with no number' | 1       | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | [new Player('No Number', null)] | []                                                          || '-n 1 -i 6 -r 2 -f 2 -p "No Number"'
    'extra player'          | 2       | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | thirteenPlayers.take(1)         | []                                                          || '-n 2 -i 6 -r 2 -f 2 -p "Player 1#1"'
    'named players'         | null    | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | thirteenPlayers.take(2)         | []                                                          || '-n 2 -i 6 -r 2 -f 2 -p "Player 1#1" -p "Player 2#2"'
    'not playing player'    | null    | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | []                              | ['Not Today']                                               || '-n 0 -i 6 -r 2 -f 2 -np "Not Today"'
    'not playing players'   | null    | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | []                              | ['Not Today', 'Includes Number#0']                          || '-n 0 -i 6 -r 2 -f 2 -np "Not Today" -np "Includes Number#0"'
    'with debug'            | 1       | 6       | false | true  | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | []                              | []                                                          || '-n 1 -i 6 -r 2 -f 2 --debug'
    'with info'             | 1       | 6       | true  | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | []                              | []                                                          || '-n 1 -i 6 -r 2 -f 2 --info'
    'debug overrides info'  | 1       | 6       | true  | true  | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | []                              | []                                                          || '-n 1 -i 6 -r 2 -f 2 --debug'
    'innings'               | 1       | 8       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | []                              | []                                                          || '-n 1 -i 8 -r 2 -f 2'
    'position counts'       | 1       | 6       | false | false | true  | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | []                              | []                                                          || '-n 1 -i 6 -r 2 -f 2 --io'
    'back-to-back check'    | 1       | 6       | false | false | false | true      | false  | false     | false     | false     | false     | false  | false | null             | null   | null   | []                              | []                                                          || '-n 1 -i 6 -r 2 -f 2 -b'
    'kinder check'          | 1       | 6       | false | false | false | false     | true   | false     | false     | false     | false     | false  | false | null             | null   | null   | []                              | []                                                          || '-n 1 -i 6 -r 2 -f 2 -k'
    'simple'                | 1       | 6       | false | false | false | false     | false  | false     | false     | false     | false     | true   | false | null             | null   | null   | []                              | []                                                          || '-n 1 -i 6 -r 2 -f 2 --simple'
    'grid'                  | 9       | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | true  | null             | null   | null   | []                              | []                                                          || '-n 9 -i 6 -r 2 -f 2 --grid'
    'label'                 | 1       | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | 'Just the label' | null   | null   | []                              | []                                                          || '-n 1 -i 6 -r 2 -f 2 -l "Just the label"'
    'just home'             | 1       | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | null   | 'Home' | []                              | []                                                          || '-n 1 -i 6 -r 2 -f 2'
    'just away'             | 1       | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | 'Away' | null   | []                              | []                                                          || '-n 1 -i 6 -r 2 -f 2'
    'home and away'         | 1       | 6       | false | false | false | false     | false  | false     | false     | false     | false     | false  | false | null             | 'Away' | 'Home' | []                              | []                                                          || '-n 1 -i 6 -r 2 -f 2 --away "Away" --home "Home"'
    'large and trues'       | 13      | 6       | true  | true  | true  | true      | true   | true      | true      | true      | true      | false  | false | 'Label Included' | null   | null   | thirteenPlayers                 | ['Keeps Order', 'First Alphabetically', 'Middle Number#13'] || '-n 13 -i 6 -r 2 -f 2 --debug --io -b -k -1 -2 -3 -4 -l "Label Included" -p "Player 1#1" -p "Player 2#2" -p "Player 3#3" -p "Player 4#4" -p "Player 5#5" -p "Player 6#6" -p "Player 7#7" -p "Player 8#8" -p "Player 9#9" -p "Player 10#10" -p "Player 11#11" -p "Player 12#12" -p "Player 13#13" -np "Keeps Order" -np "First Alphabetically" -np "Middle Number#13"'
  }

  def 'sets parameters returns messages - #description'() {
    given:
    llRosterBuilderCommand.help = help
    llRosterBuilderCommand.numberOfInnings = numberOfInnings
    llRosterBuilderCommand.numberOfPlayers = numberOfPlayers

    when:
    def message = llRosterBuilderCommand.setParameters()

    then:
    message == expectedMessage

    where:
    description        | help  | numberOfInnings | numberOfPlayers || expectedMessage
    'help stops'       | true  | 0               | 0               || 'Help requested, passed helper!'
    'too few innings'  | false | 0               | 0               || 'Number of innings exceeds allowed bounds (1-12): 0'
    'too many innings' | false | 13              | 0               || 'Number of innings exceeds allowed bounds (1-12): 13'
    'too few players'  | false | 6               | 4               || 'Number of players exceeds allowed bounds (5-15): 4'
    'too many innings' | false | 6               | 16              || 'Number of players exceeds allowed bounds (5-15): 16'
  }

  def 'sets parameters modifies other settings - #description'() {
    given:
    ((Logger) LoggerFactory.getLogger(llRosterBuilderCommand.class.packageName)).setLevel(Level.OFF)

    llRosterBuilderCommand.debug = debug
    llRosterBuilderCommand.info = info
    llRosterBuilderCommand.skipRule1 = skipRule1
    llRosterBuilderCommand.skipRule2 = skipRule2
    llRosterBuilderCommand.skipRule3 = skipRule3
    llRosterBuilderCommand.skipRule4 = skipRule4
    llRosterBuilderCommand.numberOfInnings = numberOfInnings
    llRosterBuilderCommand.numberOfPlayers = numberOfPlayers

    when:
    def message = llRosterBuilderCommand.setParameters()

    then:
    message == null

    and:
    ((Logger) LoggerFactory.getLogger(llRosterBuilderCommand.class.packageName)).effectiveLevel == expectedLevel

    llRosterBuilderCommand.skipRule1 == expectedSkipRule1
    llRosterBuilderCommand.skipRule2 == expectedSkipRule2
    llRosterBuilderCommand.skipRule3 == expectedSkipRule3
    llRosterBuilderCommand.skipRule4 == expectedSkipRule4

    and: 'Shuffles, so cannot equal'
    llRosterBuilderCommand.order.size() == expectedOrder.size()
    llRosterBuilderCommand.order.containsAll(expectedOrder)
    expectedOrder.containsAll(llRosterBuilderCommand.order)

    and:
    llRosterBuilderCommand.positions == Position.values().take(Math.min(numberOfPlayers, 10)).toList()

    where:
    description                        | debug | info  | skipRule1 | skipRule2 | skipRule3 | skipRule4 | numberOfInnings | numberOfPlayers || expectedLevel | expectedSkipRule1 | expectedSkipRule2 | expectedSkipRule3 | expectedSkipRule4 | expectedOrder
    'debug sets'                       | true  | false | false     | false     | false     | false     | 6               | 9               || Level.DEBUG   | false             | false             | false             | false             | (1..9).collect { new Player("Player ${it}", null) }
    'info sets'                        | false | true  | false     | false     | false     | false     | 6               | 9               || Level.INFO    | false             | false             | false             | false             | (1..9).collect { new Player("Player ${it}", null) }
    'debug over info'                  | true  | true  | false     | false     | false     | false     | 6               | 9               || Level.DEBUG   | false             | false             | false             | false             | (1..9).collect { new Player("Player ${it}", null) }
    'too many innings skips all rules' | false | false | false     | false     | false     | false     | 7               | 9               || Level.OFF     | true              | true              | true              | true              | (1..9).collect { new Player("Player ${it}", null) }
    'too many players skips rule 1'    | false | false | false     | false     | false     | false     | 6               | 13              || Level.OFF     | true              | false             | false             | false             | (1..13).collect { new Player("Player ${it}", null) }
    '5 players suppress rule 3 & 4'    | false | false | false     | false     | false     | false     | 5               | 5               || Level.OFF     | false             | false             | true              | true              | (1..5).collect { new Player("Player ${it}", null) }
    '8 players suppress rule 4'        | false | false | false     | false     | false     | false     | 5               | 8               || Level.OFF     | false             | false             | false             | true              | (1..8).collect { new Player("Player ${it}", null) }
  }

  def 'sets parameters builds order - #description'() {
    given:
    llRosterBuilderCommand.numberOfPlayers = numberOfPlayers
    llRosterBuilderCommand.playerNames = playerNames as List<String>
    llRosterBuilderCommand.shuffle = shuffle

    when:
    def message = llRosterBuilderCommand.setParameters()

    then:
    message == expectedMessage

    and:
    llRosterBuilderCommand.numberOfPlayers == expectedNumber
    llRosterBuilderCommand.order?.size() == expectedNames.size()
    llRosterBuilderCommand.order*.name.containsAll(expectedNames)
    if (shuffle) llRosterBuilderCommand.order*.name != expectedNames else llRosterBuilderCommand.order*.name == expectedNames

    and:
    expectedFilters.size() == llRosterBuilderCommand.filterPositions.size()
    llRosterBuilderCommand.filterPositions.containsAll(expectedFilters)

    where:
    description                      | numberOfPlayers | playerNames                           | shuffle || expectedMessage                                       | expectedNumber | expectedNames                      | expectedFilters
    'too few players sends message'  | 4               | null                                  | false   || 'Number of players exceeds allowed bounds (5-15): 4'  | 4              | []                                 | []
    'too many players sends message' | 16              | null                                  | false   || 'Number of players exceeds allowed bounds (5-15): 16' | 16             | []                                 | []
    'defaults to 9 generated'        | null            | null                                  | false   || null                                                  | 9              | thirteenPlayers.take(9)*.name      | []
    'defaults to 9 generated'        | null            | null                                  | false   || null                                                  | 9              | thirteenPlayers.take(9)*.name      | []
    'shuffles 9 generated'           | null            | null                                  | true    || null                                                  | 9              | thirteenPlayers.take(9)*.name      | []
    'makes players just by numbers'  | 5               | null                                  | false   || null                                                  | 5              | thirteenPlayers.take(5)*.name      | []
    'makes players just from names'  | null            | thirteenPlayers.takeRight(7)*.name    | false   || null                                                  | 7              | thirteenPlayers.takeRight(7)*.name | []
    'adds players to provided names' | 9               | thirteenPlayers.take(5)*.name         | false   || null                                                  | 9              | thirteenPlayers.take(9)*.name      | []
    'truncates provided names'       | 5               | thirteenPlayers.take(9)*.name         | false   || null                                                  | 5              | thirteenPlayers.take(5)*.name      | []
    'shuffles names'                 | null            | thirteenPlayers*.name                 | true    || null                                                  | 13             | thirteenPlayers*.name              | []
    'builds filter'                  | 5               | ["${thirteenPlayers.first().name};C"] | false   || null                                                  | 5              | thirteenPlayers.take(5)*.name      | [new PlayerPosition(new Player(thirteenPlayers.first().name, null), CATCHER)]
  }

  def 'sets parameters limits positions - #description'() {
    given:
    llRosterBuilderCommand.positions = []
    llRosterBuilderCommand.numberOfPlayers = numberOfPlayers

    when:
    def message = llRosterBuilderCommand.setParameters()

    then:
    !message

    and:
    llRosterBuilderCommand.positions.size() == expectedSize
    !llRosterBuilderCommand.positions.contains(BENCH)

    where:
    description                | numberOfPlayers || expectedSize
    'minimum players'          | 5               || 5
    'full squad'               | 9               || 9
    'full little league squad' | 10              || 10
    'too many players'         | 11              || 10

  }

  def 'generate stops at help'() {
    given:
    llRosterBuilderCommand.help = true

    expect: 'Ensures generate() calls setParameters()'
    'Help requested, passed helper!' == llRosterBuilderCommand.generate()
  }

  def 'generate with grid - #description'() {
    given:
    llRosterBuilderCommand.grid = true

    // Ignores these
    llRosterBuilderCommand.order = thirteenPlayers
    llRosterBuilderCommand.playerNamesNotAttending = thirteenPlayers*.name
    llRosterBuilderCommand.positions = [PITCHER, RIGHT]

    llRosterBuilderCommand.away = away
    llRosterBuilderCommand.home = home
    llRosterBuilderCommand.label = label
    llRosterBuilderCommand.numberOfInnings = numberOfInnings
    llRosterBuilderCommand.numberOfPlayers = numberOfPlayers

    def lineChecking = 0

    def expectedHeader = "|| No |       Player       |"
    (1..numberOfInnings).each { expectedHeader += " ${('   ' + (it))[-3..-1]}  |" }
    expectedHeader += '|'

    when:
    def generated = llRosterBuilderCommand.generate()
    def lines = generated.readLines()

    then:
    lines.size() == expected.lineCount

    and:
    if (label || (away && home)) {
      /*
        ++-------------------------------------------------------------------++
        ||                               Label                               ||
      */
      assert lines.first.matches(/\+\+-*\+\+/)

      if (label)
        assert lines[1].contains(label)
      else
        assert lines[1].contains("** $away * at * $home **")

      lineChecking = 2
    }

    and:
    /*
      ++----+--------------------+------+------+------+------+------+------++
      || No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||
      ++----+--------------------+------+------+------+------+------+------++
    */
    lines[lineChecking].matches(/[\\+-]*/)
    lines[lineChecking].count('+') == 5 + numberOfInnings

    lines[++lineChecking] == expectedHeader

    lines[++lineChecking].matches(/[\\+-]*/)
    lines[lineChecking].count('+') == 5 + numberOfInnings

    and:
    /*
      ++----+--------------------+------+------+------+------+------+------++
      ||    |                    |      |      |      |      |      |      ||
      ++----+--------------------+------+------+------+------+------+------++
    */
    lines.findAll { it.matches(/\|\| {4}\| {20}\|( {6}\|)*\|/) }.size() == numberOfPlayers
    lines.findAll { it.matches(/\+\+-{4}\+-{20}\+(-{6}\+)*\+/) }.size() == expected.separators

    and:
    if (away && home) {
      /*
        ++====+====================+======+======+======+======+======+======++
        || Away                    |      |      |      |      |      |      ||
        ++-------------------------+------+------+------+------+------+------++
        || Home                    |      |      |      |      |      |      ||
        ++-------------------------+------+------+------+------+------+------++
      */
      assert lines[-5].matches(/\+\+(=*\+)*\+/)
      assert lines[-4].startsWith("|| $away")
      assert lines[-3].matches(/[\\+-]*/)
      assert lines[-2].startsWith("|| $home")

      assert lines.last.count('+') == 4 + numberOfInnings
    } else {
      /*
        ++----+--------------------+------+------+------+------+------+------++
      */
      lines.last.count('+') == 5 + numberOfInnings
    }

    lines.last.matches(/[\\+-]*/)

    where:
    description        | numberOfPlayers | numberOfInnings | label               | away        | home        || expected
    'default'          | 9               | 6               | null                | null        | null        || [lineCount: 21, separators: 11]
    'fewer players'    | 6               | 6               | null                | null        | null        || [lineCount: 15, separators: 8]
    'more players'     | 13              | 6               | null                | null        | null        || [lineCount: 29, separators: 15]
    'fewer innings'    | 9               | 5               | null                | null        | null        || [lineCount: 21, separators: 11]
    'more innings'     | 9               | 12              | null                | null        | null        || [lineCount: 21, separators: 11]
    'label'            | 9               | 6               | 'Label for Testing' | null        | null        || [lineCount: 23, separators: 11]
    'teams'            | 9               | 6               | null                | 'Away Team' | 'Home Team' || [lineCount: 27, separators: 10]
    'teams with label' | 7               | 6               | 'Label for Testing' | 'Away Team' | 'Home Team' || [lineCount: 23, separators: 8]
  }

  def 'try generation retries when zero keeps innings - #description'() {
    given:
    llRosterBuilderCommand.order = [] // No players should skip generation when retried

    List<Inning> innings = []
    innings << new Inning(playerPositions: [new PlayerPosition(thirteenPlayers.first(), LEFT)])
    innings << new Inning(playerPositions: [new PlayerPosition(thirteenPlayers.first(), RIGHT)])

    when:
    def generation = llRosterBuilderCommand.tryGeneration(innings, retryCount)

    then:
    generation.startsWith('Unable to fill a complete roster. This is the last attempt.')

    and:
    innings*.playerPositions*.position.flatten() == expectedPositions

    where:
    description                      | retryCount || expectedPositions
    'already 0, no more retries'     | 0          || [LEFT, RIGHT]
    '1 should clear, suppressed gen' | 1          || []
  }

  def 'try generation simple ordering - #description'() {
    given:
    llRosterBuilderCommand.numberOfInnings = 1
    llRosterBuilderCommand.numberOfPlayers = numberOfPlayers
    llRosterBuilderCommand.order = (1..numberOfPlayers).collect { new Player("Player $it", it) }
    llRosterBuilderCommand.positions = Position.values().take(numberOfPlayers)
    llRosterBuilderCommand.simple = true

    List<Inning> innings = []

    when:
    llRosterBuilderCommand.tryGeneration(innings, 1)

    then:
    innings.size() == 1
    innings.first().playerPositions.size() == numberOfPlayers

    and:
    innings.first().playerPositions*.position == expectedPositions

    where:
    description               | numberOfPlayers || expectedPositions
    'as small as possible'    | 5               || [PITCHER, FIRST, SECOND, THIRD, SHORT]
    'too few skips catcher'   | 7               || [PITCHER, FIRST, SECOND, THIRD, SHORT, LEFT, RIGHT]
    'enough includes catcher' | 8               || [PITCHER, CATCHER, FIRST, SECOND, THIRD, SHORT, LEFT, RIGHT]
    'full field'              | 9               || [PITCHER, CATCHER, FIRST, SECOND, THIRD, SHORT, LEFT, LEFT_CENTER, RIGHT]
    'full LL field'           | 10              || [PITCHER, CATCHER, FIRST, SECOND, THIRD, SHORT, LEFT, LEFT_CENTER, RIGHT, RIGHT_CENTER]
    'bench last'              | 11              || [PITCHER, CATCHER, FIRST, SECOND, THIRD, SHORT, LEFT, LEFT_CENTER, RIGHT, RIGHT_CENTER, BENCH]
    'extra bench last'        | 13              || [PITCHER, CATCHER, FIRST, SECOND, THIRD, SHORT, LEFT, LEFT_CENTER, RIGHT, RIGHT_CENTER, BENCH, BENCH, BENCH]
    'as large as possible'    | 15              || [PITCHER, CATCHER, FIRST, SECOND, THIRD, SHORT, LEFT, LEFT_CENTER, RIGHT, RIGHT_CENTER, BENCH, BENCH, BENCH, BENCH, BENCH]
  }

  @Ignore
  def 'try generation retries when fails - #description'() {
    given:
    llRosterBuilderCommand.numberOfPlayers = numberOfPlayers
    llRosterBuilderCommand.numberOfInnings = numberOfInnings
    llRosterBuilderCommand.order = thirteenPlayers.take(numberOfPlayers)

    // Only outfield, to avoid success
    llRosterBuilderCommand.positions = [LEFT, LEFT_CENTER, RIGHT_CENTER, RIGHT, BENCH].take(numberOfPlayers)

    // Don't worry about other rules
    llRosterBuilderCommand.skipRule1 = true
    llRosterBuilderCommand.skipRule2 = false // Fails because all outfield
    llRosterBuilderCommand.skipRule3 = true
    llRosterBuilderCommand.skipRule4 = true

    List<Inning> innings = []

    when:
    def generation = llRosterBuilderCommand.tryGeneration(innings, 1)

    then: 'Not really wrong, just not given a chance to succeed'
    generation.startsWith('Unable to fill a complete roster. This is the last attempt.')

    and:
    innings.size() == expectedNumberOfInnings
    innings.each { it.playerPositions.size() == numberOfPlayers }

    where:
    description              | numberOfInnings | numberOfPlayers || expectedNumberOfInnings
    'two innings one player' | 2               | 1               || 1
//    'two innings two players'  | 2               | 2               || 2
//    'six innings nine players' | 6               | 9               || 6
  }

  def 'gets try positions filters positions - #description'() {
    given:
    llRosterBuilderCommand.filterPositions = filterPositions

    llRosterBuilderCommand.numberOfPlayers = 5
    llRosterBuilderCommand.order = thirteenPlayers.take(5)

    when:
    def tryPositions = llRosterBuilderCommand.getTryPositions([], availablePositions, thirteenPlayers.first())

    then:
    tryPositions == expectedPositions

    where:
    description               | availablePositions        | filterPositions                                                                                    || expectedPositions
    'no filters'              | [PITCHER, CATCHER]        | []                                                                                                 || [PITCHER, CATCHER]
    'player filters'          | [PITCHER, CATCHER]        | [new PlayerPosition(thirteenPlayers[0], CATCHER)]                                                  || [PITCHER]
    'player filters multiple' | [PITCHER, CATCHER, FIRST] | [new PlayerPosition(thirteenPlayers[0], PITCHER), new PlayerPosition(thirteenPlayers[0], CATCHER)] || [FIRST]
    'not player no filters'   | [PITCHER, CATCHER]        | [new PlayerPosition(thirteenPlayers[1], CATCHER)]                                                  || [PITCHER, CATCHER]
  }

  def 'gets try positions removes back-to-back - #description'() {
    given:
    llRosterBuilderCommand.backToBackCheck = backToBack

    llRosterBuilderCommand.numberOfPlayers = 5
    llRosterBuilderCommand.order = thirteenPlayers.take(5)

    def innings = inningsPositions.collect { position ->
      new Inning(playerPositions: [new PlayerPosition(thirteenPlayers.first(), position)])
    }

    when:
    def tryPositions = llRosterBuilderCommand.getTryPositions(innings, availablePositions, thirteenPlayers.first())

    then:
    tryPositions == expectedPositions

    where:
    description                 | backToBack | inningsPositions | availablePositions || expectedPositions
    'not checking back-to-back' | false      | [PITCHER]        | [PITCHER, CATCHER] || availablePositions
    'checking back-to-back'     | true       | [PITCHER]        | [PITCHER, CATCHER] || [CATCHER]
    'not back-to-back'          | true       | [PITCHER, LEFT]  | [PITCHER, CATCHER] || availablePositions
  }

  def 'gets try positions removes kinder check - $description'() {
    given:
    llRosterBuilderCommand.kinderPositionCheck = kinderPosition

    llRosterBuilderCommand.numberOfPlayers = 5
    llRosterBuilderCommand.order = thirteenPlayers.take(5)

    def innings = [new Inning(playerPositions: [])]
    inningsPositions.eachWithIndex { position, index ->
      innings.first().playerPositions << new PlayerPosition(thirteenPlayers[index], position)
    }

    when:
    def tryPositions = llRosterBuilderCommand.getTryPositions(innings, availablePositions, thirteenPlayers.first())

    then:
    tryPositions == expectedPositions

    where:
    description                 | kinderPosition | inningsPositions                         | availablePositions || expectedPositions
    'checking kinder found'     | true           | [PITCHER, CATCHER, FIRST, SECOND, THIRD] | [PITCHER, SHORT]   || [SHORT]
    'checking kinder not found' | true           | [PITCHER, CATCHER, FIRST, SECOND, THIRD] | [SHORT, LEFT]      || availablePositions
    'not checking kinder'       | false          | [PITCHER, CATCHER, FIRST, SECOND, THIRD] | [PITCHER, CATCHER] || availablePositions
  }

  def 'gets try positions checks bench - #description'() {
    given:
    llRosterBuilderCommand.numberOfPlayers = numberOfPlayers

    // Don't filter on other rules
    llRosterBuilderCommand.skipRule1 = true
    llRosterBuilderCommand.skipRule3 = true
    llRosterBuilderCommand.skipRule4 = true

    def innings = inningsPositions.collect { Position position ->
      new Inning(playerPositions: [new PlayerPosition(thirteenPlayers.first(), position)])
    }

    when:
    def tryPositions = llRosterBuilderCommand.getTryPositions(innings, availablePositions, thirteenPlayers.first())

    then:
    tryPositions == expectedPositions

    where:
    description                           | numberOfPlayers | inningsPositions                       | availablePositions                       || expectedPositions
    'too few players'                     | 10              | [PITCHER, CATCHER, LEFT, FIRST, RIGHT] | [PITCHER, FIRST, SECOND, THIRD, CATCHER] || availablePositions
    'already offering bench'              | 10              | [PITCHER, CATCHER, LEFT, FIRST, RIGHT] | [PITCHER, FIRST, SECOND, THIRD, BENCH]   || availablePositions
    'not yet played bench'                | 11              | [PITCHER, CATCHER, LEFT, FIRST, RIGHT] | [PITCHER, FIRST, SECOND, THIRD, CATCHER] || availablePositions + BENCH
    'not yet played bench adds just once' | 13              | [PITCHER, CATCHER, LEFT, FIRST, RIGHT] | [PITCHER, FIRST, SECOND, THIRD, CATCHER] || availablePositions + BENCH
    'already played bench enough'         | 11              | [PITCHER, CATCHER, BENCH, LEFT, RIGHT] | [PITCHER, FIRST, SECOND, THIRD, CATCHER] || availablePositions
    'already played bench and can again'  | 12              | [PITCHER, CATCHER, BENCH, LEFT, RIGHT] | [PITCHER, FIRST, SECOND, THIRD, CATCHER] || availablePositions + BENCH
    'already played bench adds just once' | 13              | [PITCHER, CATCHER, BENCH, LEFT, RIGHT] | [PITCHER, FIRST, SECOND, THIRD, CATCHER] || availablePositions + BENCH
    'only bench'                          | 13              | [PITCHER, CATCHER, BENCH, LEFT, RIGHT] | []                                       || [BENCH]
    'last played bench'                   | 13              | [PITCHER, CATCHER, LEFT, RIGHT, BENCH] | [PITCHER, FIRST, SECOND, THIRD, CATCHER] || availablePositions
  }

}
