package com.swbyjeff.llrosterbuilder

import com.swbyjeff.llrosterbuilder.domain.Inning
import com.swbyjeff.llrosterbuilder.domain.Player
import com.swbyjeff.llrosterbuilder.domain.PlayerPosition
import com.swbyjeff.llrosterbuilder.domain.Position
import spock.lang.Shared
import spock.lang.Specification

import static com.swbyjeff.llrosterbuilder.domain.Position.BENCH
import static com.swbyjeff.llrosterbuilder.domain.Position.CATCHER
import static com.swbyjeff.llrosterbuilder.domain.Position.FIRST
import static com.swbyjeff.llrosterbuilder.domain.Position.LEFT
import static com.swbyjeff.llrosterbuilder.domain.Position.LEFT_CENTER
import static com.swbyjeff.llrosterbuilder.domain.Position.PITCHER
import static com.swbyjeff.llrosterbuilder.domain.Position.RIGHT
import static com.swbyjeff.llrosterbuilder.domain.Position.RIGHT_CENTER
import static com.swbyjeff.llrosterbuilder.domain.Position.SECOND
import static com.swbyjeff.llrosterbuilder.domain.Position.SHORT
import static com.swbyjeff.llrosterbuilder.domain.Position.THIRD

class LLRosterBuilderSpec extends Specification {

  LLRosterBuilder llRosterBuilder = new LLRosterBuilder()

  @Shared
  def allPositions = List.of(Position.values())

  @Shared
  def ninePositions = allPositions.take(9)

  @Shared
  def tenPositions = allPositions.take(10)

  @Shared
  def players = (1..13).collect { new Player("Player ${it}", it) }

  @Shared
  // 6-inning, 10-player game with all players in the same position each inning...
  def basicGame = (1..6).collect { new Inning(number: it, playerPositions: players.take(10).collect { new PlayerPosition(it, tenPositions[players.indexOf(it)]) }) }

  def 'prints innings - #description'() {
    given:
    def innings = [] as List<Inning>

    playerPositions.each { key, value ->
      def player = (order as List<Player>).find { it.number == key }
      if (player) {
        value.eachWithIndex { position, number ->
          def inning = innings.find { it.number == number }
          if (!inning) {
            inning = new Inning(number: number)
            innings << inning
          }
          inning.playerPositions << new PlayerPosition(player, position)
        }
      }
    }

    when:
    def printedInnings = llRosterBuilder.printInnings(order, innings, label, printInOut, [], null, null)

    then:
    expected.each {
      assert printedInnings.contains(it)
    }

    and:
    if (label) {
      assert printedInnings.readLines().first().matches(/\+\+-*\+\+/)
    } else {
      assert printedInnings.readLines().first().matches(/\+\+(-+\+)+-+\+\+/)
    }
    printedInnings.findAll(/\+\+(-+\+)+-+\+\+/).size() == order.size() + 2

    and:
    printedInnings.contains('| No |       Player       |')

    where:
    description                    | order                                                                                                                                        | playerPositions                                                                                                | label                           | printInOut || expected
    'Truncates too long'           | []                                                                                                                                           | [:]                                                                                                            | 'This is much too long to keep' | false      || ['|| This is much too long t ||']
    'Centers label long'           | []                                                                                                                                           | [:]                                                                                                            | 'This pretty full'              | false      || ['||    This pretty full     ||']
    'Centers label odd'            | []                                                                                                                                           | [:]                                                                                                            | 'Seven'                         | false      || ['||          Seven          ||']
    'Centers label even'           | []                                                                                                                                           | [:]                                                                                                            | 'Four'                          | false      || ['||          Four           ||']
    'Centers label with inning'    | [new Player('One', 1)]                                                                                                                       | [1: [PITCHER]]                                                                                                 | 'Has One Inning'                | false      || ['||         Has One Inning         |', '|   1  ||']
    'Centers label with in/out'    | [new Player('One', 1)]                                                                                                                       | [1: [PITCHER, LEFT]]                                                                                           | 'Includes in/out for 2 innings' | true       || ['||          Includes in/out for 2 innings           ||', '|| No |       Player       |   1  |   2  | In/Out/B ||', '||  1 | One                |   P  |  LF  |  1/1/0   ||']
    'Centers label with 2 innings' | [new Player('One', 1)]                                                                                                                       | [1: [PITCHER, CATCHER]]                                                                                        | 'Has Two Innings'               | false      || ['||            Has Two Innings            ||', '|| No |       Player       |   1  |   2  ||']
    'Centers label with 6 innings' | [new Player('One', 1)]                                                                                                                       | [1: [PITCHER, CATCHER, FIRST, SECOND, THIRD, SHORT]]                                                           | 'Has Six Innings'               | false      || ['||                          Has Six Innings                          ||', '|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  ||', '||  1 | One                |   P  |   C  |  1B  |  2B  |  3B  |  SS  ||']
    'Lists 2 players with in/out'  | [new Player('One', 1), new Player('Two', 2)]                                                                                                 | [1: [PITCHER, CATCHER, FIRST, SECOND, THIRD, SHORT], 2: [CATCHER, RIGHT, SECOND, LEFT_CENTER, SHORT, PITCHER]] | null                            | true       || ['|| No |       Player       |   1  |   2  |   3  |   4  |   5  |   6  | In/Out/B ||', '||  1 | One                |   P  |   C  |  1B  |  2B  |  3B  |  SS  |  6/0/0   ||', '||  2 | Two                |   C  |  RF  |  2B  |  LC  |  SS  |   P  |  4/2/0   ||']
    'Lists 6 players no innings'   | [new Player('One', 1), new Player('Twos', 22), new Player('Three', 3), new Player('Fours', 44), new Player('Five', 5), new Player('Six', 6)] | [:]                                                                                                            | null                            | false      || ['|| No |       Player       ||', '||  1 | One                ||', '| 22 | Twos               |', '|  3 | Three              |', '| 44 | Fours              |', '|  5 | Five               |', '|  6 | Six                |']
    'Lists 6 players with in/out'  | [new Player('One', 1), new Player('Twos', 22), new Player('Three', 3), new Player('Fours', 44), new Player('Five', 5), new Player('Six', 6)] | [1: [PITCHER, BENCH, RIGHT, FIRST], 22: [LEFT, RIGHT, LEFT_CENTER], 44: [BENCH, PITCHER, LEFT, CATCHER]]       | null                            | true       || ['|| No |       Player       |   1  |   2  |   3  |   4  | In/Out/B ||', '||  1 | One                |   P  |   B  |  RF  |  1B  |  2/1/1   ||', '|| 22 | Twos               |  LF  |  RF  |  LC  |      |  0/3/0   ||', '||  3 | Three              |      |      |      |      |  0/0/0   ||', '|| 44 | Fours              |   B  |   P  |  LF  |   C  |  2/1/1   ||', '||  5 | Five               |      |      |      |      |  0/0/0   ||', '||  6 | Six                |      |      |      |      |  0/0/0   ||']
    'Truncates long player name'   | [new Player('Quextorilian Pentabulous', 321)]                                                                                                | [321: [PITCHER]]                                                                                               | null                            | false      || ['|| No |       Player       |   1  ||', '||321 | Quextorilian Penta |   P  ||']
  }

  def 'prints innings with players not attending - #description'() {
    when:
    def printedInnings = llRosterBuilder.printInnings([], innings, null, printInOut, playersNotAttending, null, null)

    then: 'Fairly, in a normal game, players would not be attending and not attending at the same time'
    expected.each { string ->
      assert printedInnings.contains(string)
    }

    where:
    description              | innings           | printInOut | playersNotAttending             || expected
    'no innings'             | []                | false      | [players[0]]                    || ["||  ${players[0].number} | ${players[0].name.padRight(18)} ||"]
    'no innings with in/out' | []                | true       | [players[0]]                    || ["||  ${players[0].number} | ${players[0].name.padRight(18)} |          ||"]
    'one inning'             | basicGame.take(1) | false      | [players[1]]                    || ["||  ${players[1].number} | ${players[1].name.padRight(18)} |      ||"]
    'one inning with in/out' | basicGame.take(1) | true       | [players[1]]                    || ["||  ${players[1].number} | ${players[1].name.padRight(18)} |      |          ||"]
    'six innings'            | basicGame         | false      | [players[6]]                    || ["||  ${players[6].number} | ${players[6].name.padRight(18)} |      |      |      |      |      |      ||"]
    'six inning with in/out' | basicGame         | true       | [players[6]]                    || ["||  ${players[6].number} | ${players[6].name.padRight(18)} |      |      |      |      |      |      |          ||"]
    'two not playing'        | basicGame         | false      | [players[6], players[8]]        || ["||  ${players[6].number} | ${players[6].name.padRight(18)} |      |      |      |      |      |      ||", "||  ${players[8].number} | ${players[8].name.padRight(18)} |      |      |      |      |      |      ||"]
    'player with no number'  | basicGame.take(1) | false      | [new Player('Just Name', null)] || ["||    | Just Name          |      ||"]
  }

  def 'prints innings with away and home teams - #description'() {
    when:
    def printInnings = llRosterBuilder.printInnings(players, basicGame, label, false, [], away, home)
    def printInningLines = printInnings.readLines()

    then:
    if (expectedLabel) {
      assert printInningLines.first == '++-------------------------------------------------------------------++'
      assert printInningLines[1].contains(expectedLabel)
      assert printInningLines[2] == ('++----+--------------------+------+------+------+------+------+------++')
    } else {
      assert printInningLines.first == ('++----+--------------------+------+------+------+------+------+------++')
    }

    and:
    if (away && home) {
      def lastLines = printInningLines.takeRight(5)
      assert lastLines.first == '++====+====================+======+======+======+======+======+======++'
      assert lastLines[1].startsWith(expectedAway)
      assert lastLines[2] == '++-------------------------+------+------+------+------+------+------++'
      assert lastLines[3].startsWith(expectedHome)
    } else if (away) {
      assert !printInnings.contains(away)
    } else if (home) {
      assert !printInnings.contains(home)
    }
    printInningLines.last == expectedEnd

    where:
    description     | label         | away   | home   || expectedLabel            | expectedAway | expectedHome | expectedEnd
    'no things'     | null          | null   | null   || null                     | null         | null         | '++----+--------------------+------+------+------+------+------+------++'
    'labels work'   | 'Still Works' | null   | null   || '    Still Works    '    | null         | null         | '++----+--------------------+------+------+------+------+------+------++'
    'missing away'  | null          | null   | 'Home' || null                     | null         | null         | '++----+--------------------+------+------+------+------+------+------++'
    'missing home'  | null          | 'Away' | null   || null                     | null         | null         | '++----+--------------------+------+------+------+------+------+------++'
    'uses label'    | 'Game On!'    | 'Away' | 'Home' || '    Game On!    '       | '|| Away'    | '|| Home'    | '++-------------------------+------+------+------+------+------+------++'
    'creates label' | ''            | 'Away' | 'Home' || '** Away * at * Home **' | '|| Away'    | '|| Home'    | '++-------------------------+------+------+------+------+------+------++'
  }

  def 'lists players - #description'() {
    expect:
    expected == llRosterBuilder.listPlayers(names)

    where:
    description               | names                                  || expected
    'null names'              | null                                   || []
    'empty names'             | []                                     || []
    'name without #'          | ['Just Name']                          || [new Player('Just Name', null)]
    'names without #'         | ['First Name', 'Second Name']          || [new Player('First Name', null), new Player('Second Name', null)]
    'name with #'             | ['Numbered Name#1']                    || [new Player('Numbered Name', 1)]
    'names with #'            | ['Numbered Name#1', 'Numbered Name#2'] || [new Player('Numbered Name', 1), new Player('Numbered Name', 2)]
    'name with ##'            | ['What Happens#1#2']                   || [new Player('What Happens#1#2', null)]
    'name with space after #' | ['What Happens# 12']                   || [new Player('What Happens', 12)]
  }

  def 'lists players adds filters - #description'() {
    given:
    List<PlayerPosition> filterPositions = []

    when:
    def listed = llRosterBuilder.listPlayers(inputs, filterPositions)

    then:
    listed.containsAll(expectedPlayers)
    filterPositions.containsAll(expectedFilters)

    where:
    description                   | inputs                                                                                      || expectedPlayers          | expectedFilters
    'one player one filter'       | ["${players[0].name}#${players[0].number};C"]                                               || [players[0]]             | [new PlayerPosition(players[0], CATCHER)]
    'one player two filters'      | ["${players[1].name}#${players[1].number};C;1B"]                                            || [players[1]]             | [new PlayerPosition(players[1], CATCHER), new PlayerPosition(players[1], FIRST)]
    'two players one filter each' | ["${players[2].name}#${players[2].number};C", "${players[3].name}#${players[3].number};1B"] || [players[2], players[3]] | [new PlayerPosition(players[2], CATCHER), new PlayerPosition(players[3], FIRST)]
    'two players one filter only' | ["${players[4].name}#${players[4].number};C", "${players[5].name}#${players[5].number}"]    || [players[4], players[5]] | [new PlayerPosition(players[4], CATCHER)]
  }

  def 'gets player positions - #description'() {
    given:

    expect:
    expected == llRosterBuilder.getPlayerPositions(innings, player)

    where:
    description        | innings                                                                                                                                                                                                                                                                   | player                     || expected
    'null innings'     | null                                                                                                                                                                                                                                                                      | players[0]                 || []
    'empty innings'    | []                                                                                                                                                                                                                                                                        | players[0]                 || []
    'null player'      | basicGame                                                                                                                                                                                                                                                                 | null                       || [null, null, null, null, null, null]
    'not found player' | basicGame                                                                                                                                                                                                                                                                 | new Player('Missing#0', 0) || [null, null, null, null, null, null]
    'found player 1'   | basicGame                                                                                                                                                                                                                                                                 | players[0]                 || [PITCHER, PITCHER, PITCHER, PITCHER, PITCHER, PITCHER]
    'found player 3'   | basicGame                                                                                                                                                                                                                                                                 | players[2]                 || [SECOND, SECOND, SECOND, SECOND, SECOND, SECOND]
    'found player 6'   | basicGame                                                                                                                                                                                                                                                                 | players[5]                 || [LEFT, LEFT, LEFT, LEFT, LEFT, LEFT]
    'found player 8'   | basicGame                                                                                                                                                                                                                                                                 | players[7]                 || [CATCHER, CATCHER, CATCHER, CATCHER, CATCHER, CATCHER]
    'found player 9'   | basicGame                                                                                                                                                                                                                                                                 | players[8]                 || [LEFT_CENTER, LEFT_CENTER, LEFT_CENTER, LEFT_CENTER, LEFT_CENTER, LEFT_CENTER]
    'single inning'    | [basicGame[0]]                                                                                                                                                                                                                                                            | players[0]                 || [PITCHER]
    'couple innings'   | [basicGame[0], basicGame[2]]                                                                                                                                                                                                                                              | players[1]                 || [FIRST, FIRST]
    'mixed innings'    | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], ninePositions[3])]), new Inning(number: 2, playerPositions: [new PlayerPosition(players[0], BENCH)]), new Inning(number: 3, playerPositions: [new PlayerPosition(players[0], ninePositions[6])])] | players[0]                 || [THIRD, BENCH, RIGHT]
    'missing innings'  | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], ninePositions[3])]), new Inning(number: 2, playerPositions: []), new Inning(number: 3, playerPositions: [new PlayerPosition(players[0], ninePositions[6])])]                                      | players[0]                 || [THIRD, null, RIGHT]
  }

  def 'gets valid positions - #description'() {
    given:
    def availablePositions = List.of(Position.values().take(numberOfPlayers))

    when:
    def tryPositions = llRosterBuilder.getValidPositions(availablePositions, alreadyPlayed, skipRule1, skipRule3, skipRule4)

    then:
    tryPositions.size() == expected.size()
    tryPositions.containsAll(expected)

    where:
    description                  | alreadyPlayed             | skipRule1 | skipRule3 | skipRule4 | numberOfPlayers || expected
    'first inning'               | []                        | false     | false     | false     | 9               || ninePositions
    'first inning small team'    | []                        | false     | false     | false     | 6               || allPositions.take(6)
    'first inning large team'    | []                        | false     | false     | false     | 10              || tenPositions
    'first inning large4 team'   | []                        | false     | false     | false     | 11              || allPositions
    'already PITCHER'            | [PITCHER]                 | false     | false     | false     | 9               || ninePositions
    'filters rule 1'             | [BENCH, RIGHT, BENCH]     | false     | false     | false     | 11              || tenPositions
    'skips rule 1'               | [BENCH, BENCH, RIGHT]     | true      | false     | false     | 11              || allPositions
    'filters rule 3'             | [PITCHER, RIGHT, PITCHER] | false     | false     | false     | 9               || ninePositions - [PITCHER]
    'skips rule 3'               | [PITCHER, RIGHT, PITCHER] | false     | true      | false     | 9               || ninePositions
    'filters rule 3 large team'  | [PITCHER, RIGHT, PITCHER] | false     | false     | false     | 10              || tenPositions - [PITCHER]
    'filters rule 3 larger team' | [PITCHER, RIGHT, PITCHER] | false     | false     | false     | 11              || allPositions - [PITCHER]
    'filters rule 4'             | [PITCHER, CATCHER]        | false     | false     | false     | 9               || [LEFT, LEFT_CENTER, RIGHT]
    'skips rule 4'               | [PITCHER, CATCHER]        | false     | false     | true      | 9               || ninePositions
    'filters rule 4 large team'  | [PITCHER, CATCHER]        | false     | false     | false     | 10              || [LEFT, LEFT_CENTER, RIGHT_CENTER, RIGHT]
    'filters rule 4 larger team' | [PITCHER, CATCHER]        | false     | false     | false     | 11              || [LEFT, LEFT_CENTER, RIGHT_CENTER, RIGHT, BENCH]
    'just adds one BENCH'        | []                        | false     | false     | false     | 12              || allPositions
    'can be BENCH again'         | [BENCH]                   | false     | false     | false     | 12              || allPositions
    'already BENCH twice'        | [BENCH, BENCH]            | false     | false     | false     | 12              || tenPositions
    'already BENCH enough'       | [BENCH, BENCH]            | false     | false     | false     | 13              || tenPositions
  }

  def 'violates rule 1 - #description'() {
    expect:
    expected == llRosterBuilder.violatesRule1(inningPositions, testPosition)

    where:
    description          | inningPositions              | testPosition || expected
    'missing position'   | [FIRST]                      | null         || false
    'not bench'          | [BENCH, BENCH, BENCH, BENCH] | PITCHER      || false
    'first inning'       | []                           | BENCH        || false
    'second inning'      | [BENCH]                      | BENCH        || false
    'third inning'       | [BENCH, PITCHER]             | BENCH        || false
    'no previous bench'  | [SHORT, RIGHT, FIRST, LEFT]  | BENCH        || false
    'one previous bench' | [BENCH, RIGHT, FIRST, LEFT]  | BENCH        || false
    'two previous bench' | [BENCH, FIRST, BENCH, LEFT]  | BENCH        || true
    'way too many'       | [BENCH, BENCH, BENCH, BENCH] | BENCH        || true
  }

  def 'validates rule 2 - #description'() {
    given:
    List<Inning> innings = []
    inningPositions.eachWithIndex { positions, positionIndex ->
      if (innings.size() < positions.size()) {
        while (innings.size() < positions.size()) {
          innings << new Inning(number: innings.size() + 1)
        }
      }

      positions.eachWithIndex { position, inningIndex ->
        def inning = innings.get(inningIndex)
        def player = players[positionIndex]
        inning.playerPositions << new PlayerPosition(player, position)
      }
    }

    expect:
    expected == llRosterBuilder.validateRule2(innings, players.take(inningPositions.size()))

    where:
    description                | inningPositions                                                                                                || expected
    'one inning'               | [[LEFT], [RIGHT], [LEFT_CENTER], [RIGHT_CENTER]]                                                               || true
    'two innings mixed'        | [[LEFT, PITCHER], [RIGHT, CATCHER], [LEFT_CENTER, FIRST], [RIGHT_CENTER, SECOND]]                              || true
    'two innings infield'      | [[FIRST, PITCHER], [SECOND, CATCHER], [THIRD, FIRST], [SHORT, SECOND]]                                         || false
    'three innings mixed'      | [[LEFT, FIRST, PITCHER], [SECOND, LEFT_CENTER, CATCHER], [THIRD, RIGHT_CENTER, FIRST], [SHORT, SECOND, RIGHT]] || false
    'one player all infield'   | [[PITCHER, CATCHER, FIRST, SECOND, THIRD, SHORT]]                                                              || false
    'one player all outfield'  | [[LEFT, LEFT_CENTER, RIGHT_CENTER]]                                                                            || true
    'two players all outfield' | [[LEFT, RIGHT], [LEFT_CENTER, RIGHT_CENTER]]                                                                   || true
    'one player one infield'   | [[PITCHER, LEFT, LEFT_CENTER, RIGHT_CENTER]]                                                                   || true
    'one player mixed'         | [[PITCHER, CATCHER, LEFT_CENTER, RIGHT_CENTER]]                                                                || false
    'two players mixed'        | [[PITCHER, CATCHER, LEFT_CENTER, RIGHT_CENTER], [CATCHER, LEFT_CENTER, RIGHT_CENTER, PITCHER]]                 || false
  }

  def 'violates rule 3 - #description'() {

    expect:
    expected == llRosterBuilder.violatesRule3(inningPositions, testPosition)

    where:
    description                               | inningPositions                    | testPosition || expected
    'first inning'                            | []                                 | PITCHER      || false
    'no position'                             | [PITCHER, PITCHER]                 | null         || false
    'ignores bench (checked by rule 1)'       | [BENCH, PITCHER, BENCH, FIRST]     | BENCH        || false
    'not the same'                            | [PITCHER]                          | CATCHER      || false
    'second time'                             | [PITCHER, CATCHER]                 | CATCHER      || false
    'third time'                              | [CATCHER, CATCHER]                 | CATCHER      || true
    'same position once in previous innings'  | [CATCHER, PITCHER, FIRST, SECOND]  | CATCHER      || false
    'same position twice in previous innings' | [CATCHER, PITCHER, CATCHER, FIRST] | CATCHER      || true
  }

  def 'violates rule 4 - #description'() {
    expect:
    expected == llRosterBuilder.violatesRule4(ninePositions, testPosition)

    where:
    description                  | ninePositions                | testPosition | expected
    'first innings'              | []                           | FIRST        | false
    'second inning'              | [FIRST]                      | SECOND       | false
    'no position'                | [FIRST, SECOND]              | null         | false
    'position is bench'          | [FIRST, SECOND]              | BENCH        | false
    'position in outfield'       | [FIRST, SECOND]              | RIGHT_CENTER | false
    'third infield fails'        | [FIRST, SECOND]              | THIRD        | true
    'second infield OK'          | [LEFT, FIRST]                | SECOND       | false
    'infield after outfield OK'  | [FIRST, RIGHT]               | SECOND       | false
    'one infield before bench'   | [THIRD, BENCH]               | PITCHER      | false
    'two infield before bench'   | [SECOND, THIRD, BENCH]       | PITCHER      | true
    'bench between infield'      | [SECOND, BENCH, THIRD]       | PITCHER      | true
    'ignores previous violation' | [FIRST, THIRD, SHORT, RIGHT] | PITCHER      | false
  }

  def 'violates back-to-back - #description'() {
    expect:
    expected == llRosterBuilder.violatesBackToBack(playerPositions, testPosition)

    where:
    description                               | playerPositions  | testPosition || expected
    'no positions'                            | []               | FIRST        || false
    'no position'                             | [FIRST]          | null         || false
    'last inning same position'               | [PITCHER]        | PITCHER      || true
    'last inning also bench'                  | [BENCH]          | BENCH        || true
    'last inning different infield position'  | [PITCHER]        | CATCHER      || false
    'last inning different outfield position' | [LEFT]           | RIGHT        || true
    'last inning not outfield position'       | [SHORT]          | RIGHT        || false
    'infield before bench to infield'         | [PITCHER, BENCH] | CATCHER      || false
    'infield before bench to outfield'        | [PITCHER, BENCH] | RIGHT        || false
    'outfield before bench to infield'        | [LEFT, BENCH]    | FIRST        || false
    'outfield before bench to outfield'       | [LEFT, BENCH]    | RIGHT        || true
  }

  def 'violates other players first - #description'() {
    given:
    def i = 1
    def innings = inningPositions?.collect { positions ->
      new Inning(number: i++, playerPositions: positions.collect { new PlayerPosition(players[positions.indexOf(it)], it) })
    }

    expect:
    expected == llRosterBuilder.violatesOtherPlayersFirst(playerPositions, testPosition, player, order, innings)

    where:
    description                 | playerPositions           | testPosition | player     | order           | inningPositions                                                                   || expected
    'mull positions'            | []                        | PITCHER      | players[3] | players.take(3) | [[PITCHER, CATCHER, FIRST]]                                                       || false
    'empty positions'           | []                        | CATCHER      | players[2] | players.take(3) | [[PITCHER, CATCHER, FIRST]]                                                       || false
    'null position'             | allPositions              | null         | players[1] | players.take(3) | [[PITCHER, CATCHER, FIRST]]                                                       || false
    'null player'               | allPositions              | FIRST        | null       | players.take(3) | [[PITCHER, CATCHER, FIRST]]                                                       || false
    'null order'                | allPositions              | FIRST        | players[1] | null            | [[PITCHER, CATCHER, FIRST]]                                                       || false
    'empty order'               | allPositions              | FIRST        | players[2] | []              | [[PITCHER, CATCHER, FIRST]]                                                       || false
    'null innings'              | allPositions              | THIRD        | players[3] | players.take(3) | null                                                                              || false
    'empty innings'             | allPositions              | SHORT        | players[0] | players.take(3) | []                                                                                || false
    'new position'              | [PITCHER, CATCHER, FIRST] | SECOND       | players[0] | players.take(3) | [[PITCHER, CATCHER, FIRST], [CATCHER, FIRST, PITCHER], [FIRST, PITCHER, CATCHER]] || false
    'repeat others all played'  | [PITCHER, CATCHER, FIRST] | PITCHER      | players[0] | players.take(3) | [[PITCHER, CATCHER, FIRST], [CATCHER, FIRST, PITCHER], [FIRST, PITCHER, CATCHER]] || false
    'repeat no one else played' | [PITCHER, CATCHER, FIRST] | PITCHER      | players[0] | players.take(3) | [[PITCHER, CATCHER, FIRST], [CATCHER, FIRST, SECOND], [FIRST, THIRD, CATCHER]]    || true
    'repeat someone not played' | [PITCHER, CATCHER, FIRST] | PITCHER      | players[0] | players.take(3) | [[PITCHER, CATCHER, FIRST], [CATCHER, FIRST, SECOND], [FIRST, PITCHER, CATCHER]]  || true
  }

  def 'validates complete game - #description'() {
    expect:
    expected == llRosterBuilder.validateCompleteGame(innings, order, usePositions, numberOfInnings)

    where:
    description                  | innings                                                                                                                                                                                                                                                  | numberOfInnings | usePositions       | order           || expected
    'null innings'               | null                                                                                                                                                                                                                                                     | 0               | []                 | []              || false
    'no innings expected'        | []                                                                                                                                                                                                                                                       | 0               | []                 | []              || true
    'not enough innings'         | []                                                                                                                                                                                                                                                       | 1               | []                 | []              || false
    'one inning no players'      | [new Inning()]                                                                                                                                                                                                                                           | 1               | []                 | players.take(1) || false
    'one inning one player'      | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], PITCHER)])]                                                                                                                                                                      | 1               | [PITCHER]          | players.take(1) || true
    'one inning two players'     | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], PITCHER), new PlayerPosition(players[1], CATCHER)])]                                                                                                                             | 1               | [PITCHER, CATCHER] | players.take(2) || true
    'one inning same player'     | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], PITCHER), new PlayerPosition(players[0], CATCHER)])]                                                                                                                             | 1               | [PITCHER, CATCHER] | players.take(2) || false
    'ine inning same position'   | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], PITCHER), new PlayerPosition(players[1], PITCHER)])]                                                                                                                             | 1               | [PITCHER, CATCHER] | players.take(2) || false
    'two innings one player'     | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], PITCHER)]), new Inning(number: 2, playerPositions: [new PlayerPosition(players[0], PITCHER)])]                                                                                   | 2               | [PITCHER]          | players.take(1) || true
    'two innings two players'    | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], PITCHER), new PlayerPosition(players[1], CATCHER)]), new Inning(number: 2, playerPositions: [new PlayerPosition(players[1], PITCHER), new PlayerPosition(players[0], CATCHER)])] | 2               | [PITCHER, CATCHER] | players.take(2) || true
    'two innings missing player' | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], PITCHER), new PlayerPosition(players[1], CATCHER)]), new Inning(number: 2, playerPositions: [new PlayerPosition(players[1], PITCHER)])]                                          | 2               | [PITCHER, CATCHER] | players.take(2) || false
    'not enough players'         | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], PITCHER)])]                                                                                                                                                                      | 1               | [PITCHER, CATCHER] | players.take(2) || false
    'unexpected position'        | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], PITCHER)])]                                                                                                                                                                      | 1               | [CATCHER]          | players.take(1) || false
    'missing player'             | [new Inning(number: 1, playerPositions: [new PlayerPosition(players[0], PITCHER)])]                                                                                                                                                                      | 1               | [PITCHER]          | [players[1]]    || false
  }

  def 'validates complete game with innings and players - #numberOfPlayers'() {
    given:
    def positions = Position.values().take(numberOfPlayers).toList()

    def innings = (1..6).collect { number ->
      def usePositions = positions.shuffled()
      usePositions.addAll(BENCH, BENCH, BENCH)
      usePositions = usePositions.take(numberOfPlayers)
      new Inning(number: number, playerPositions: players.take(numberOfPlayers).shuffled().collect { new PlayerPosition(it, usePositions.removeFirst()) })
    }

    expect: 'ignores rules but all players and positions should be in each inning'
    llRosterBuilder.validateCompleteGame(innings, players.take(numberOfPlayers), positions, innings.size())

    where:
    numberOfPlayers << (6..13)
  }

  def 'validates complete game BENCH rules - #description'() {
    given:
    def innings = [new Inning(number: 1, playerPositions: validatePositions.collect { position -> new PlayerPosition(null, position) })]
    innings[0].playerPositions.eachWithIndex { playerPosition, i -> playerPosition.player = players[i] }

    expect:
    expected == llRosterBuilder.validateCompleteGame(innings, players.take(numberOfPlayers), validatePositions.toList(), 1)

    where:
    description                 | numberOfPlayers | validatePositions                                  || expected
    'no BENCH 9 players'        | 9               | Position.values().take(9)                          || true
    'too many BENCH 9 players'  | 9               | Position.values().take(8) + [BENCH]                || false
    'no BENCH 10 players'       | 10              | Position.values().take(10)                         || true
    'too many BENCH 10 players' | 10              | Position.values().take(9) + [BENCH]                || false
    'enough BENCH 11 players'   | 11              | Position.values().take(10) + [BENCH]               || true
    'enough BENCH 12 players'   | 12              | Position.values().take(10) + [BENCH, BENCH]        || true
    'enough BENCH 13 players'   | 13              | Position.values().take(10) + [BENCH, BENCH, BENCH] || true
  }
}
