package com.swbyjeff.llrosterbuilder.domain

class Player {
  String name
  Integer number

  Player(String name, Integer number) {
    this.name = name
    this.number = number
  }

  @Override
  boolean equals(Object object) {
    return object instanceof Player &&
        ((Player) object).toString() == toString()
  }

  @Override
  int hashCode() {
    return toString().hashCode().intValue()
  }

  @Override
  String toString() {
    return "{\"number\":${number},\"name\":\"${name}\"}"
  }
}
