package com.swbyjeff.llrosterbuilder.domain

class PlayerPosition {

  Player player
  Position position

  PlayerPosition(Player player, Position position) {
    this.player = player
    this.position = position
  }

  @Override
  boolean equals(Object object) {
    return object instanceof PlayerPosition &&
        ((PlayerPosition) object).toString() == toString()
  }

  @Override
  int hashCode() {
    return toString().hashCode().intValue()
  }

  @Override
  String toString() {
    return "{\"player\":${player},\"position\":\"${position}\"}"
  }
}
