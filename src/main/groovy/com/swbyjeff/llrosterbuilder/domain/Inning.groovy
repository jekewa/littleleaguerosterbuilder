package com.swbyjeff.llrosterbuilder.domain

class Inning {

  Integer number
  List<PlayerPosition> playerPositions = []

  @Override
  String toString() {
    return "{\"inning\":${number},\"positions\":${playerPositions}]"
  }
}
