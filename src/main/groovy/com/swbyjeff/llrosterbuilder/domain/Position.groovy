package com.swbyjeff.llrosterbuilder.domain

enum Position {

  PITCHER(1, 'P', 'Pitcher', true),
  FIRST(3, '1B', 'First Base', true),
  SECOND(4, '2B', 'Second Base', true),
  THIRD(5, '3B', 'Third Base', true),
  SHORT(6, 'SS', 'Short Stop', true),
  LEFT(7, 'LF', 'Left Field', false),
  RIGHT(9, 'RF', 'Right Field', false),
  CATCHER(2, 'C', 'Catcher', true),
  LEFT_CENTER(8, 'LC', '(Left) Center Field', false),
  RIGHT_CENTER(10, 'RC', 'Right Center Field', false),
  BENCH(11, 'B', 'Bench', false, true);

  Integer code
  String id
  String title
  boolean infield
  boolean bench

  Position(int code, String id, String title, boolean infield, bench = false) {
    this.code = code
    this.id = id
    this.title = title
    this.infield = infield
    this.bench = bench
  }

  boolean isOutfield() {
    return !bench && !infield
  }
}