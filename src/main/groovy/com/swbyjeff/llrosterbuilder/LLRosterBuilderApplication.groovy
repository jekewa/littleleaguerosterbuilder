package com.swbyjeff.llrosterbuilder

import groovy.transform.CompileStatic
import io.micronaut.runtime.Micronaut

@CompileStatic
class LLRosterBuilderApplication {

  static void main(String[] args) {

    Micronaut.build(args)
        .mainClass(LLRosterBuilderApplication)
    //    .packages('com.swbyjeff.llorosterbuilder','com.swbyjeff.llorosterbuilder.domain')
        .banner(true)
        .start()
  }
}
