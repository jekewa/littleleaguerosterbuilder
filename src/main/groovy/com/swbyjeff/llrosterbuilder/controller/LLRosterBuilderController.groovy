package com.swbyjeff.llrosterbuilder.controller

import com.swbyjeff.llrosterbuilder.LLRosterBuilderCommand
import io.micronaut.core.annotation.Nullable
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.QueryValue

@Controller('/llrosterbuilder')
@Produces(MediaType.TEXT_PLAIN)
@SuppressWarnings('GrMethodMayBeStatic')
class LLRosterBuilderController extends BaseController {

  @Get('/roster')
  HttpResponse<String> getRoster(@QueryValue("player") @Nullable List<String> players,
                                 @QueryValue("skip") @Nullable List<String> skipPlayers,
                                 @QueryValue("label") @Nullable String label,
                                 @QueryValue("shuffle") @Nullable Boolean shuffle,
                                 @QueryValue("players") @Nullable Integer numberOfPlayers,
                                 @QueryValue("innings") @Nullable Integer innings) {

    def llRosterBuilderCommand = new LLRosterBuilderCommand()

    if (players) llRosterBuilderCommand.playerNames = players
    if (skipPlayers) llRosterBuilderCommand.playerNamesNotAttending = skipPlayers
    if (label) llRosterBuilderCommand.label = label

    llRosterBuilderCommand.numberOfPlayers = numberOfPlayers ?: 9
    llRosterBuilderCommand.numberOfInnings = innings ?: 6
    llRosterBuilderCommand.shuffle = shuffle
    llRosterBuilderCommand.retryLimit = 5
    llRosterBuilderCommand.failLimit = 10000

    return HttpResponse.ok(llRosterBuilderCommand.generate())
  }
}
