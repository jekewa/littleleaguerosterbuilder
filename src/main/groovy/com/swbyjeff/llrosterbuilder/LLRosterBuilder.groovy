package com.swbyjeff.llrosterbuilder

import com.swbyjeff.llrosterbuilder.domain.Inning
import com.swbyjeff.llrosterbuilder.domain.Player
import com.swbyjeff.llrosterbuilder.domain.PlayerPosition
import com.swbyjeff.llrosterbuilder.domain.Position
import groovy.util.logging.Slf4j
import jakarta.inject.Singleton

@Slf4j
@Singleton
@SuppressWarnings('GrMethodMayBeStatic')
class LLRosterBuilder {
  String printInnings(List<Player> order, List<Inning> innings, String label, Boolean printInOut, List<Player> playersNotAttending, String away, String home) {

    if (!label && away && home) {
      label = "** $away * at * $home **"
    }
    def inningPrint = ''
    if (label?.trim()) {
      inningPrint = '++--------------------------'
      innings.size().times { inningPrint += '-------' }
      if (printInOut) inningPrint += '-----------'
      inningPrint = inningPrint.take(inningPrint.length() - 1) + '++'

      def headerLength = inningPrint.length() - 6

      def centeredLabel = label.take(headerLength).trim()
      if (centeredLabel.length() < headerLength) {
        int half = BigInteger.valueOf(headerLength - centeredLabel.length()).divide(BigInteger.TWO).intValue()
        centeredLabel = centeredLabel.padLeft(half + centeredLabel.length())
        centeredLabel = centeredLabel.padRight(headerLength).take(headerLength)
      }
      inningPrint += "\n|| ${centeredLabel} ||\n"
    }

    inningPrint += printSeparator(innings.size(), printInOut).takeAfter('\n')

    inningPrint += '\n|| No |       Player       |'
    innings.size().times { inningPrint += " ${('   ' + (it + 1))[-3..-1]}  |" }
    if (printInOut) inningPrint += ' In/Out/B |'
    inningPrint += '|'

    for (Player player : order) {
      inningPrint += printSeparator(innings.size(), printInOut)

      int infield = 0
      int outfield = 0
      int bench = 0
      inningPrint += "\n||${(player.number?.toString() ?: '').padLeft(3).takeRight(3)} | ${player.name.padRight(18).take(18)} |"

      def playerPositions = getPlayerPositions(innings, player)
      playerPositions.each { position ->
        if (position) {
          inningPrint += (position.id.padLeft(4)) + '  |'
          if (position.infield) infield++ else if (position.bench) bench++ else outfield++
        } else {
          inningPrint += '      |'
        }
      }
      if (printInOut) inningPrint += "  ${infield}/${outfield}/${bench}   |"
      inningPrint += '|'
    }

    if (playersNotAttending) {
      for (Player player : playersNotAttending) {
        inningPrint += printSeparator(innings.size(), printInOut)

        inningPrint += "\n|| ${(player.number?.toString() ?: '').padLeft(2)} | ${player.name.padRight(18)} |"
        innings.size().times { inningPrint += '      |' }
        if (printInOut) inningPrint += '          |'
        inningPrint += '|'
      }
    }

    if (away && home) {
      inningPrint += printSeparator(innings.size(), printInOut).replaceAll("-", "=")

      [away, home].each { team ->
        inningPrint += "\n|| ${team.padRight(23).take(23)} |"
        innings.size().times { inningPrint += '      |' }
        if (printInOut) inningPrint += '          |'
        inningPrint += '|'

        inningPrint += '\n++-------------------------+'
        innings.size().times { inningPrint += '------+' }
        if (printInOut) inningPrint += '----------+'
        inningPrint += '+'
      }
    } else {
      inningPrint += printSeparator(innings.size(), printInOut)
    }

    return inningPrint
  }

  private String printSeparator(int inningSize, boolean printInOut) {
    String separator = '\n++----+--------------------+'
    inningSize.times { separator += '------+' }
    if (printInOut) separator += '----------+'
    return separator + '+'
  }

  // Process "Name(s)#Number;Filter(s)
  List<Player> listPlayers(List<String> players, List<PlayerPosition> filterPositions = []) {
    return players?.collect { info ->

      def filters = info.takeAfter(';')
      if (filters) {
        info = info.takeBefore(';')
      }

      Integer number = null
      if (info.contains('#') && info.takeAfter('#').isNumber()) {
        number = info.takeAfter('#').toInteger()
        info = info.takeBefore("#")
      }
      def player = new Player(info, number)

      filters?.split(';')?.each { id ->
        def position = Position.values().find { it.id == id }
        if (position) filterPositions << new PlayerPosition(player, position)
      }

      return player
    } ?: []
  }

  List<Position> getPlayerPositions(List<Inning> innings, Player player) {
    def positions = innings?.collect { inning -> inning.playerPositions.find { it.player == player }?.position
    } as List<Position>
    return positions ?: []
  }

  List<Position> getValidPositions(List<Position> inningPositions, List<Position> alreadyPlayed, boolean skipRule1, boolean skipRule3, boolean skipRule4) {
    def tryPositions = inningPositions.toSorted()

    // Remove positions that violate rules
    if (!skipRule1) tryPositions.removeAll { violatesRule1(alreadyPlayed, it) }
    // Rule 2 evaluated at end of roster building
    if (!skipRule3) tryPositions.removeAll { violatesRule3(alreadyPlayed, it) }
    if (!skipRule4) tryPositions.removeAll { violatesRule4(alreadyPlayed, it) }

    return tryPositions
  }

  // No more than 2 BENCH innings
  boolean violatesRule1(List<Position> playerPositions, Position position) {
    if (!playerPositions || !position || !position.bench) return false

    // One already plus this one are allowed
    def benchInnings = playerPositions.count { it?.bench }.intValue()
    return benchInnings > 1
  }

  // Player should play at least 2 infield innings
  boolean validateRule2(List<Inning> innings, List<Player> order) {

    // Iterate through innings to ensure all players have at least 2 infield positions during the game
    def rule2Violations = order?.findAll { player ->
      def playerPositions = getPlayerPositions(innings, player)
      def infieldPositions = playerPositions.count { it?.infield }.intValue()
      return infieldPositions < 2 ? player : null
    }

    if (rule2Violations) {
      log.debug("R2: Found ${rule2Violations.size()} violations for ${rule2Violations*.name}")
      return true
    }
    return false
  }

  // No more than 2 innings in the same position
  boolean violatesRule3(List<Position> playerPositions, Position position) {
    // BENCH is not a position...
    if (!position || position.bench) return false

    // One already plus this one are allowed
    def positionInnings = playerPositions.count(position).intValue()
    return positionInnings > 1
  }

// No more than 2 consecutive infield innings
  boolean violatesRule4(List<Position> playerPositions, Position position) {
    if (playerPositions.size() > 1 && position?.infield) {
      def lastPositions = playerPositions.takeRight(2)
      if (lastPositions.contains(Position.BENCH) && playerPositions.size() > 2) {
        lastPositions = playerPositions.takeRight(3)
        lastPositions.removeAll { it?.bench }
      }
      if (lastPositions.size() > 1 && lastPositions.first()?.infield && lastPositions.last()?.infield) {
        return true
      }
    }
    return false
  }

  boolean violatesBackToBack(List<Position> playerPositions, Position position) {
    if (playerPositions && position) {

      def playerLastInning = playerPositions.last

      // Ensure no back-to-back playing of same positions (or bench)
      if (playerLastInning == position) {
        return true
      }

      // Not back-to-back outfield
      if (playerLastInning?.outfield && position?.outfield) {
        return true
      }

      // Check the inning previous if last inning is bench (no P B P or O B O ... or O B B O)
      if (playerLastInning?.bench && playerPositions.size() > 1) {
        return violatesBackToBack(playerPositions.take(playerPositions.size() - 1), position)
      }
    }
    return false
  }


  // Is there a position already played such that another player in a previous inning hasn't played it yet?
  boolean violatesOtherPlayersFirst(List<Position> playerPositions, Position position, Player player, List<Player> order, List<Inning> innings) {
    boolean violated = false
    if (position && playerPositions?.contains(position) && player && order && innings) {
      (order - player).each { batter ->
        def played = getPlayerPositions(innings, batter)?.contains(position)
        if (!played) violated = true
      }
    }

    return violated
  }

  boolean validateCompleteGame(List<Inning> innings, List<Player> order, List<Position> positions, Integer expectedInnings) {
    def validCompleteGame = true

    // Ensure the right number of innings
    if (innings?.size() != expectedInnings) {
      log.info("GC: Incorrect number of innings: have ${innings?.size()} expected ${expectedInnings}")
      validCompleteGame = false
    } else {
      innings.each { inning ->

        if (validCompleteGame) {
          // Ensure all innings have enough players in position
          if (inning?.playerPositions?.findAll()?.size() != order.size()) {
            log.info("GC: Not enough player positions in ${inning.number}: ${inning.playerPositions*.position}")
            validCompleteGame = false
          } else if (!inning.playerPositions*.player?.containsAll(order)) {
            log.info("GC: Not all players in inning ${inning.number}: ${inning.playerPositions*.player}")
            validCompleteGame = false
          } else if (!inning.playerPositions*.position.containsAll(positions)) {
            log.info("GC: not all positions in ${inning.number}: ${inning.playerPositions*.position}")
            validCompleteGame = false
          } else if (inning.playerPositions.count { it?.position?.bench }.intValue() != Math.max(order.size() - 10, 0)) {
            log.info("GC: Too many times for BENCH in ${inning.number}")
            validCompleteGame = false
          }
        }
      }
    }

    return validCompleteGame
  }

}
