package com.swbyjeff.llrosterbuilder

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.Logger
import com.swbyjeff.llrosterbuilder.domain.*
import groovy.util.logging.Slf4j
import io.micronaut.configuration.picocli.PicocliRunner
import jakarta.inject.Inject
import org.slf4j.LoggerFactory
import picocli.CommandLine
import picocli.CommandLine.Option

@Slf4j
@CommandLine.Command(name = 'LLRosterBuilder', description = 'Quick line-up and position randomizer for little league baseball teams.')
class LLRosterBuilderCommand implements Runnable {

  // Rules
  // 1) Bench Time - no more than 2 innings
  // 2) At Least 2 Innings Infield
  // 3) Same Position - no more than 2 innings (really same as rule 1...)
  // 4) Consecutive Infield Innings - no more than 2 innings

  List<Player> order = []
  List<Position> positions = []
  List<PlayerPosition> filterPositions = []

  @Option(names = ['--debug'], negatable = true, description = 'Turn on debug-level messaging, which can be very chatty.')
  Boolean debug = Boolean.FALSE

  @Option(names = ['--info'], negatable = true, description = 'Turn on info-level messaging, which can be a little chatty.')
  Boolean info = Boolean.FALSE

  @Option(names = ['--io', '--inout'], negatable = true, description = 'Print count of infield and outfield innings for each player.')
  boolean printInOut = false

  @Option(names = ['-p', '--player'], description = 'List of players', split = ',', defaultValue = Option.NULL_VALUE)
  List<String> playerNames

  @Option(names = ['-np', '--not-playing'], description = 'List of players not attending, but still listed on the roster', split = ',', defaultValue = Option.NULL_VALUE)
  List<String> playerNamesNotAttending

  @Option(names = ['-n', '--numberOfPlayers'], description = 'Number of players to use (1-12)', defaultValue = Option.NULL_VALUE)
  Integer numberOfPlayers

  @Option(names = ['-i', '--numberOfInnings'], description = 'Number of innings to plan (1-15)', defaultValue = '6')
  Integer numberOfInnings

  @Option(names = ['-b', '--backToBackCheck'], description = 'Keep players from playing the same position twice in a row')
  Boolean backToBackCheck = Boolean.FALSE

  @Option(names = ['-k', '--kinderPositionCheck'], description = 'Allow kinder position checking, keeping fair levels')
  Boolean kinderPositionCheck = Boolean.FALSE

  @Option(names = ['-s', '--shuffle'], negatable = true, description = 'Shuffle player order from input')
  Boolean shuffle = Boolean.FALSE

  @Option(names = ['-1', '--no1'], description = 'Ignore rule #1, no more than 2 bench innings, skipping fair levels')
  Boolean skipRule1 = Boolean.FALSE

  @Option(names = ['-2', '--no2'], description = 'Ignore rule #2, at least 2 infield innings, skipping fair levels')
  Boolean skipRule2 = Boolean.FALSE

  @Option(names = ['-3', '--no3'], description = 'Ignore rule #3, no more than 2 innings in same position')
  Boolean skipRule3 = Boolean.FALSE

  @Option(names = ['-4', '--no4'], description = 'Ignore rule #4, limit 2 consecutive infield innings')
  Boolean skipRule4 = Boolean.FALSE

  @Option(names = ['-r', '--retry'], description = 'Number of times to retry for full roster', defaultValue = '5')
  Integer retryLimit

  @Option(names = ['-f', '--fail'], description = 'Number of times to allow rule failures to be retried in roster run', defaultValue = '10000')
  Integer failLimit

  @Option(names = ['-h', '--help'], usageHelp = true, description = 'Show help')
  Boolean help = Boolean.FALSE

  @Option(names = ['-l', '--label'], description = 'Header to add to printout')
  String label

  @Option(names = ['-c', '--commands'], description = 'Print the command line parameters needed to run again with the same batting order; positions may end up different')
  Boolean commands = Boolean.FALSE

  @Option(names = ['--away'], description = 'Add the away team for printing the scoreboard area')
  String away

  @Option(names = ['--home'], description = 'Add the home team for printing the scoreboard area')
  String home

  @Option(names = ['--simple'], description = 'Put the batting order in position order for the first inning')
  Boolean simple = Boolean.FALSE

  @Option(names = ['--grid'], description = 'Print scoring grid without positions or names')
  Boolean grid = Boolean.FALSE

  @Inject
  LLRosterBuilder llRosterBuilder

  static void main(String[] args) {
    System.exit(PicocliRunner.execute(LLRosterBuilderCommand, args))
  }

  LLRosterBuilderCommand() {}

  void run() {
    System.out.println(generate())
    if (commands) System.out.println(recreateCommandLine())
  }

  String recreateCommandLine() {
    def commandLine = "-n ${numberOfPlayers ?: order?.size() ?: 0} -i ${numberOfInnings} -r ${retryLimit} -f ${failLimit}"
    if (debug) commandLine += ' --debug' else if (info) commandLine += ' --info'
    if (printInOut) commandLine += ' --io'
    if (simple) commandLine += ' --simple'
    if (backToBackCheck) commandLine += ' -b'
    if (kinderPositionCheck) commandLine += ' -k'
    if (skipRule1) commandLine += ' -1'
    if (skipRule2) commandLine += ' -2'
    if (skipRule3) commandLine += ' -3'
    if (skipRule4) commandLine += ' -4'
    // Do not add "shuffle" so batting order will be maintained with the below items
    if (label) commandLine += " -l \"${label}\""
    if (home && away) commandLine += " --away \"${away}\" --home \"${home}\""

    if (grid) commandLine += ' --grid'

    // TODO: Add filterPositions output
    if (order) order.each { player -> commandLine += ' -p "' + player.name + (player.number != null ? "#$player.number" : '') + '"' }
    if (playerNamesNotAttending) llRosterBuilder.listPlayers(playerNamesNotAttending).each { player -> commandLine += ' -np "' + player.name + (player.number != null ? "#$player.number" : '') + '"' }

    return commandLine
  }

  String setParameters() {
    if (help) return 'Help requested, passed helper!'

    if (debug) startDebugging() else if (info) startInfo()

    if (numberOfInnings < 1 || numberOfInnings > 12) {
      return "Number of innings exceeds allowed bounds (1-12): ${numberOfInnings}"
    }
    if (numberOfInnings > 6) {
      log.info('Number of innings exceeds maximum for most rules: ' + numberOfInnings)
      skipRule1 = true
      skipRule2 = true
      skipRule3 = true
      skipRule4 = true
    }

    List<Player> players = llRosterBuilder.listPlayers(playerNames, filterPositions)
    if (players && !numberOfPlayers)
      numberOfPlayers = players.size()
    else if (!players && !numberOfPlayers)
      numberOfPlayers = 9

    if (numberOfPlayers < 5 || numberOfPlayers > 15) {
      return "Number of players exceeds allowed bounds (5-15): ${numberOfPlayers}"
    }
    order = players.take(Math.max(0, Math.min(numberOfPlayers, players.size()))) as List<Player>
    while (numberOfPlayers > order.size()) {
      order.add(new Player("Player ${order.size() + 1}", null))
    }
    if (shuffle) order.shuffle()
    numberOfPlayers = order.size()

    // Rules don't work with some player numbers
    if (numberOfPlayers > 12) skipRule1 = true
    if (numberOfPlayers == 5) skipRule3 = true
    if (numberOfPlayers < 9) skipRule4 = true

    positions.addAll(Position.values().take(Math.min(numberOfPlayers, 10)))
    return null
  }

  def startDebugging() {
    ((Logger) LoggerFactory.getLogger(this.class.packageName)).setLevel(Level.DEBUG)
    log.debug('Debug requested; much logging follows')
  }

  def startInfo() {
    ((Logger) LoggerFactory.getLogger(this.class.packageName)).setLevel(Level.INFO)
    log.info('Info requested; more logging follows')
  }

  String generate() {
    String message = setParameters()
    if (message) return message

    if (grid) return llRosterBuilder.printInnings([new Player('', null)] * numberOfPlayers, [new Inning()] * numberOfInnings, label, printInOut, [], away, home)

    log.debug("Trying ${retryLimit} times for ${numberOfPlayers} players in ${numberOfInnings} innings")

    return tryGeneration(retryLimit)
  }

  // Member because of re-entrant...fix that
  String tryGeneration(int retryCount) {
    List<Inning> innings = []
    return tryGeneration(innings, retryCount)
  }

  String tryGeneration(List<Inning> innings, int retryCount) {
    if (--retryCount < 0) {
      return "Unable to fill a complete roster. This is the last attempt.\n${llRosterBuilder.printInnings(order, innings, label, printInOut, llRosterBuilder.listPlayers(playerNamesNotAttending).sort { it.name }, away, home)}"
    }

    log.debug("Setting up try #${retryCount}")

    boolean retry = false
    innings.clear()

    numberOfInnings.times { inningNumber ->
      List<PlayerPosition> playerPositions = []

      log.debug("Setting up try #${retryCount} inning ${inningNumber + 1}")


      int failCount = 0
      if (simple && inningNumber == 0) {
        def simplePositions = positions.sort { it.code }
        order.eachWithIndex { player, index ->
          if (index + 1 >= Position.BENCH.code) {
            playerPositions.add(new PlayerPosition(player, Position.BENCH))
          } else {
            playerPositions.add(new PlayerPosition(player, simplePositions[index]))
          }
        }
      } else {
        do {
          playerPositions.clear()

          List<Position> inningPositions = positions.toSorted()

          order.each { player ->

            List<Position> tryPositions = getTryPositions(innings, inningPositions, player)
            if (tryPositions) {
              def position = tryPositions.shuffled().first()
              log.debug("Trying ${position.title} for ${player.name} in inning ${inningNumber + 1}")

              playerPositions.add(new PlayerPosition(player, position))

              if (!position.bench || playerPositions*.position.count { it.bench }.intValue() >= (numberOfPlayers - 10)) {
                inningPositions.remove(position)
              }
            } else {
              log.debug("Unable to find position for ${player.name} in inning ${inningNumber + 1} attempt $failCount")
            }
          }

          // Verify each viable position filled
          if (playerPositions.size() == numberOfPlayers) {
            positions.each { position ->
              if (!playerPositions.find { it.position == position }) {
                log.debug("Cannot find $position in inning ${inningNumber + 1} attempt $failCount")
                playerPositions.clear()
              }
            }
          }
        } while (playerPositions.size() < numberOfPlayers && ++failCount < failLimit)
      }

      if (playerPositions) innings.add(new Inning(number: inningNumber + 1, playerPositions: playerPositions))
    }

    // Validate rule #2 -- ensure each player in infield twice or more
    if (!retry && llRosterBuilder.validateRule2(innings, order)) {
      log.info('Rule #2 violation!')
      retry = true
    }

    if (!retry && !llRosterBuilder.validateCompleteGame(innings, order, positions, numberOfInnings)) {
      log.info('Incomplete game!')
      retry = true
    }

    // Print innings
    if (retry) {
      log.debug("Retrying #$retryCount\n" + llRosterBuilder.printInnings(order, innings, label, printInOut, llRosterBuilder.listPlayers(playerNamesNotAttending).sort { it.name }, away, home))
      return tryGeneration(innings, retryCount)
    }
    return llRosterBuilder.printInnings(order, innings, label, printInOut, llRosterBuilder.listPlayers(playerNamesNotAttending).sort {
      it.name
    }, away, home)
  }

  List<Position> getTryPositions(List<Inning> innings, List<Position> availablePositions, Player player) {
    // Keep players from playing the same field position more than once
    List<Position> alreadyPlayed = llRosterBuilder.getPlayerPositions(innings, player)
    List<Position> tryPositions = llRosterBuilder.getValidPositions(availablePositions, alreadyPlayed, skipRule1, skipRule3, skipRule4)

    // If player hasn't been on the bench enough, and wasn't last inning, and we need bench players, add it to this players potential list
    if (!tryPositions.contains(Position.BENCH) &&
        alreadyPlayed.count { it?.bench }.intValue() < (numberOfPlayers - 10) &&
        (alreadyPlayed.empty ||
            !llRosterBuilder.violatesBackToBack(alreadyPlayed, Position.BENCH)) &&
        !llRosterBuilder.violatesOtherPlayersFirst(alreadyPlayed, Position.BENCH, player, order, innings)) {
      tryPositions << Position.BENCH
    }

    tryPositions.removeAll { position ->
      // Remove all positions filtered from player
      filterPositions.contains(new PlayerPosition(player, position))
    }

    if (backToBackCheck) {
      // Remove back-to-back positions
      tryPositions.removeAll { llRosterBuilder.violatesBackToBack(alreadyPlayed, it) }
    }

    if (kinderPositionCheck && (tryPositions - tryPositions.findAll { llRosterBuilder.violatesOtherPlayersFirst(alreadyPlayed, it, player, order, innings) })) {
      // Try to remove positions not played by other players instead of counting maximums
      tryPositions.removeAll { llRosterBuilder.violatesOtherPlayersFirst(alreadyPlayed, it, player, order, innings) }
    }

    return tryPositions
  }
}